﻿using UnityEngine;
using System.Collections; 

public class Time : MonoBehaviour {

	public GameObject loseText;
	private float time = 240;
	private bool c = false;
	public GameObject canvasHealth;


	// Use this for initialization
	void Start () 
	{
		UnityEngine.Time.timeScale = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {

		if(time <= 0)
		{
			time = 0;
			loseText.SetActive(true);
			UnityEngine.Time.timeScale = 0.0f;
		}

		
	}

	void moreTime(){
		time = time + 30;
	}


	void Crono()
	{
		time = time - 1;
		Invoke ("Crono", 1);
		UnityEngine.Time.timeScale = 1.0f;
	}

	void OnGUI(){

		if (c == true) 
		{
			canvasHealth.SendMessage("Time", time, SendMessageOptions.DontRequireReceiver);
			//GUI.Label(new Rect(5,0,100,100) , "Time Left: " + time  ); 
		}

		
	}

	void OnTriggerEnter(Collider c){
		
		if (c.gameObject.tag == "Time") 
		{
			Invoke ("Crono", 0);
			Invoke ("TimeStart",0);
		}
		
	}

	void TimeStart()
	{
		c = true;
	}


}
