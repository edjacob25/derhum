﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour {
	public GameObject balls;
	public GameObject intro;
	// Use this for initialization
	void Start () {
		if (Variables.instructions == false)
		{
			intro.SetActive(true);
		}
		else
		{
			balls.SetActive(true);
		}

	}

	public void Initialize()
	{
		//Debug.Log("No mms");
		//gameObject.GetComponentInChildren<UILabel>().text = "Encoge tu destino";
		GameObject.Find("Proposition").GetComponent<UILabel>().text ="Vuelve a elegir. No me derrotaras";
	}

	IEnumerator initial()
	{
		//Debug.Log("aqui");
		if(Variables.instructions == false){
			//Debug.Log("aqui");
			yield return Input.GetKeyDown(KeyCode.Space);
			Variables.instructions = true;
		}
		GameObject.Find("Instructions").SetActive(false);
		balls.SetActive(true);
		yield break;
	}

	// Update is called once per frame
	void Update () {
		if(Variables.instructions == false){
			if(Input.GetKeyDown(KeyCode.Space))
			{
				Variables.instructions = true;
				GameObject.Find("Instructions").SetActive(false);
				balls.SetActive(true);
			}
		}
	}
}
