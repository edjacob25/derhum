﻿using UnityEngine;
using System.Collections;

public class LabelStatus : MonoBehaviour {
	public GameObject points;
	// Use this for initialization
	void Start () {
		points.GetComponent<UILabel>().text = "Felicidades "+Variables.name+" conseguiste "+Variables.level*100+" Derechitos, muy bien";
		if (Variables.winner){
			GetComponent<UILabel>().color = Color.green;
			GetComponent<UILabel>().text = "¡Ganador!";
		}
		else {
			GetComponent<UILabel>().color = Color.red;
			GetComponent<UILabel>().text = "¡Has perdido!";
		}
		StartCoroutine("SetMoney");
	}

	IEnumerator SetMoney()
	{
		WWW www;
		WWWForm form;
		form = new WWWForm();
		string target = "http://www.jbrr25.meximas.com/addMoney.php";
		form.AddField("username",Variables.name);
		form.AddField("money",Variables.level*100);
		www = new WWW (target,form);
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
