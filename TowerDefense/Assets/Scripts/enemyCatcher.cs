﻿using UnityEngine;
using System.Collections;

public class enemyCatcher : MonoBehaviour {


	public int ganancia;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag == "bullet") {
		//	Debug.Log("bala golpea");
			SendMessageUpwards("hit",1,SendMessageOptions.DontRequireReceiver);
			Destroy(c.gameObject);
		}

		if (c.gameObject.tag == "bazooka")
		{
		//	Debug.Log("bazooka golpea");
			SendMessageUpwards("hit",10,SendMessageOptions.DontRequireReceiver);
			Destroy(c.gameObject);
		}
		
		if (c.gameObject.tag == "sniper")
		{
			//Debug.Log("sniper golpea");
			SendMessageUpwards("hit",5,SendMessageOptions.DontRequireReceiver);
			Destroy(c.gameObject);
		}
	}
}
