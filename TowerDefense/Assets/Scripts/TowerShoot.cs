using UnityEngine;
using System.Collections;

public class TowerShoot : MonoBehaviour 
{
	public GameObject bullet;
	public GameObject hit;
	Vector3 sumaVector;
	LineRenderer line;
	public int dineroVenta;
	PlayerStats stats;
	GameObject bulletCopy;
	GameObject hitCopy;

	// Use this for initialization
	void Start () 
	{
		line = GetComponentInChildren<LineRenderer> ();
		stats = GameObject.Find ("ControlUI").GetComponent<PlayerStats> ();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnMouseEnter()
	{
		line.enabled = true;
	}
	void OnMouseExit()
	{
		line.enabled = false;
	}

	public void Vender()
	{
		stats.AumentarDinero (dineroVenta);
	}

	void Shoot(GameObject target)
	{
		sumaVector = new Vector3(0,4.5f,0);
		Vector3 pos = this.transform.position;
		if(this.tag=="tower")
			pos.y += 1;
		bulletCopy = Instantiate(bullet, pos, Quaternion.identity) as GameObject;
		bulletCopy.SendMessage ("SetPlayer", target,SendMessageOptions.DontRequireReceiver);
		Physics.IgnoreCollision(bulletCopy.transform.root.collider, transform.root.collider);
		bulletCopy.rigidbody.AddForce(transform.TransformDirection(Vector3.forward) , ForceMode.Impulse);

		hitCopy = Instantiate(hit, bulletCopy.transform.position + sumaVector, Quaternion.identity) as GameObject;
	}
}
