﻿using UnityEngine;
using System.Collections;

public class RangoSniper : MonoBehaviour 
{
	float tiempoInicial, tiempoActual;
	
	// Use this for initialization
	void Start() 
	{
		tiempoInicial = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	void OnTriggerStay(Collider c)
	{
		if (c.gameObject.tag == "Enemy") 
		{
			tiempoActual = Time.time;
			if(tiempoActual - tiempoInicial > 3)
			{
				SendMessageUpwards("Shoot",c.gameObject,SendMessageOptions.DontRequireReceiver);
				tiempoInicial = tiempoActual;
			}
		}
	}
}
