﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	public GameObject food;
	public GameObject bomb;
	public GameObject savior;
	float nextActionTime = 0.0f;
	float period;
	float toGo;
	int direction = 0;
	// Use this for initialization
	void Start () {
		period = 2.0f/(Variables.level+1);
		RecalcPlace();
	}


	void RecalcPlace()
	{
		float range = 7;
		toGo = (Random.value - 0.5f) * range;
		if (transform.position.x < toGo)
			direction = 1;
		else
			direction = -1;
	}

	void Movement()
	{
		Vector3 lp = transform.position;
		if( lp.x <= toGo)
		{
			lp.x = lp.x + 0.05f;
			if (direction == -1)
			{
				RecalcPlace();
			}
		}
		else
		{
			lp.x = lp.x - 0.05f;
			if (direction == 1)
			{
				RecalcPlace();
			}
		}
		transform.position = lp;
	}

	void Spawning()
	{
		float prob = 0.9f - (Variables.level * 0.1f);
		Vector3 lp = transform.position;
		if (Time.timeSinceLevelLoad > nextActionTime)
		{
			nextActionTime = nextActionTime + period;
			if(Random.value > prob)
				Instantiate(bomb,new Vector3(lp.x, lp.y -1 ,lp.z),Quaternion.identity);
			else
			{
				Instantiate(food,new Vector3(lp.x, lp.y -1 ,lp.z),Quaternion.identity);
				Instantiate(savior, new Vector3(transform.position.x, -25, transform.position.z), Quaternion.identity);
			}
		}
	}

	// Update is called once per frame
	void Update () {
		Movement();
		Spawning();
	}
}
