﻿using UnityEngine;
using System.Collections;

public class RemoveDecals : MonoBehaviour {

	public RuntimeDecalsUpdater updater;
	public DS_DecalProjector proj;

	public GameObject hangWebs;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	

		if (Input.GetKeyDown (KeyCode.A)) {

			for( int i = 0 ; i < hangWebs.transform.childCount ; i++ ){

				updater.RemoveProjector( hangWebs.transform.GetChild(i).GetComponent<DS_DecalProjector>() );

			}

		}


	}
}
