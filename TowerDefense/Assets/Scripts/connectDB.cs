﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class connectDB : MonoBehaviour {
	public GameObject textGameObject,boton1,botonCorrecto,boton3,boton4;
	Text textPregunta, textBoton1, textBotonCorrecto, textBoton3, textBoton4;
	// Use this for initialization
	void Start () {
		textPregunta = textGameObject.GetComponent<Text> ();
		textBoton1 = boton1.GetComponentInChildren<Text>();
		textBotonCorrecto = botonCorrecto.GetComponentInChildren<Text>();
		textBoton3 = boton3.GetComponentInChildren<Text>();
		textBoton4 = boton4.GetComponentInChildren<Text>();
		StartCoroutine ("getQuestion");
		RandomBotones ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void question()
	{
		StartCoroutine(getQuestion());
		RandomBotones ();
	}

	public IEnumerator getQuestion()
	{
		int numero=Random.Range(1,8);
		WWW www;
		WWWForm form;
		string target = "localhost/Page/towerDQuestions.php?num="+numero;
		form = new WWWForm();
		form.AddField("question","text");
		form.AddField("username","root");
		www = new WWW (target,form);
		Debug.Log("getting Question");
		//while(!www.isDone)
		yield return www;
		
		if (www.error != null) {
			Debug.LogError (www.error);
		}
		else 
		{
			textPregunta.text = www.text;
			StartCoroutine("getAnswer",numero);
			//Debug.Log(www.text);
			//derechitosNumeros = int.Parse(www.text);
		}
		//RandomBotones ();

	}

	public IEnumerator getAnswer(int numPreg)
	{
		WWW www;
		WWWForm form;
		string target = "localhost/Page/towerDAnswers.php?num="+numPreg+"&correcto=1";
		form = new WWWForm();
		form.AddField("answer","text");
		form.AddField("username","root");
		www = new WWW (target,form);
		Debug.Log("getting Answer");
		//while(!www.isDone)
		yield return www;
		if (www.error != null) 
			Debug.LogError (www.error);
		else 
		{
			textBotonCorrecto.text = www.text;
			//Debug.Log(www.text);
			//derechitosNumeros = int.Parse(www.text);
		}
		string target2 = "localhost/Page/towerDAnswers.php?num="+numPreg+"&correcto=0";
		www = new WWW (target2,form);
		yield return www;
		if (www.error != null) 
			Debug.LogError (www.error);
		else {
			string[] answers = www.text.Split('&');
			textBoton1.text=answers[0];
			textBoton3.text=answers[1];
			textBoton4.text=answers[2];
		}

	}

	public void RandomBotones()
	{
		int cosa=Random.Range(1,4);
		Vector3 temp = botonCorrecto.transform.position;
		switch (cosa) {
		case 1:
			botonCorrecto.transform.position = boton1.transform.position;
			boton1.transform.position = temp;
			break;
		case 2:
			break;
		case 3:
			botonCorrecto.transform.position = boton3.transform.position;
			boton3.transform.position = temp;
			break;
		case 4:
			botonCorrecto.transform.position = boton4.transform.position;
			boton4.transform.position = temp;
			break;
		}
	}

	public void Correcto()
	{
		botonCorrecto.guiTexture.color = Color.green;

	}
}
