using UnityEngine;
using System.Collections;

public class InitButton : MonoBehaviour {
	public GameObject tfname;
	public GameObject tfpass;
	public GameObject message;
	public Communication c;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick()
	{
		string user = tfname.GetComponentInChildren<UILabel>().text;
		string pass = tfpass.GetComponentInChildren<UILabel>().text;
		StartCoroutine(c.Authorization(user, pass,AuthorizeFinish));
	}

	void AuthorizeFinish()
	{
		//red.SendMessage("");
		//Debug.Log("LLegue aqui");
		//Debug.Log(red.name + "  "+textYouTyped);
		if (Variables.result == "Authorized")
		{
			message.GetComponent<UILabel>().text = "";
			message.GetComponent<UILabel>().color = Color.green;
			message.GetComponent<UILabel>().text = "Bienvenido";
			//WaitForSeconds(2);
			Application.LoadLevel("inicio");
		}
		else
		{
			message.GetComponent<UILabel>().color = Color.red;
			message.GetComponent<UILabel>().text = "Usuario o contraseña incorrecta";
		}
	}
}
