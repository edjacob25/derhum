﻿using UnityEngine;
using System.Collections;

public class Sky : MonoBehaviour {
	Camera skybox;
	// Use this for initialization
	void Start () {
		skybox = gameObject.GetComponent<Camera>();
		skybox.backgroundColor = Color.black;
	}
	
	void ChangeSkyboxColor(int times) {
		if (times == 1)
			skybox.backgroundColor = new Color((90f / 255), (91f / 255), (92f / 255));
			//skybox.backgroundColor = Color.grey;

		else if (times == 2)
			skybox.backgroundColor = new Color((140f / 255), (140f / 255), (140f / 255));
			//skybox.backgroundColor = Color.white;
		else
			skybox.backgroundColor = new Color((202f / 255), (226f / 255), (253f / 255));
			//skybox.backgroundColor = Color.cyan;		
	}

	// Update is called once per frame
	void Update () {
		
	}
}
