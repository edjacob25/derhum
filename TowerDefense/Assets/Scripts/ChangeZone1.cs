using UnityEngine;
using System.Collections;

public class ChangeZone1 : MonoBehaviour {

	string dir;
	int cont=1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag == "Enemy"|| c.gameObject.tag == "EnemyBoss")
		{
			if(cont%4==2)
				dir="+z";
			else
				dir="-z";
			c.SendMessage ("CambiarDireccion", dir, SendMessageOptions.DontRequireReceiver);
			cont++;
		}
	}
}
