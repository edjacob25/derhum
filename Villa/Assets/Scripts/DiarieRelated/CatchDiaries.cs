﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

public class CatchDiaries : MonoBehaviour {

	public GameObject diariesPanel;
	public GameObject diariesMenu;
    public GameObject[] diaries;
    public GameObject background;
    public UILabel instructObj;
    public UILabel titleObj;
    public UILabel contentObj;
    string instruct = "";
    string title = "";
    string content = "";
    int actual = -1;
    bool into = false;
    bool read= false;
    string[,] text = new string[30,2];
    public TextAsset archive;

    void OnTriggerEnter(Collider c) {
        string[] a;
        int num = -1;
        if (c.tag == "Diaries")
        {
            into = true;
            a = c.name.Split(new char[] { 's' });
            num = int.Parse(a[1]);
            actual = num;

        }
        if (num > -1)
        {
            instruct = "Presiona F para leer el diario";
            
        }
        //Debug.Log(num);
    }

    void OnTriggerStay(Collider c) {
        if (c.tag == "Diaries")
        {
           
        }
    }

    void OnTriggerExit(Collider c) {
        if (c.tag == "Diaries")
        {
            //CleanDiary();
        }
    }

    void ReadDiary()
    {
        
        background.gameObject.SetActive(true);
        title = text[actual-1,0];
        content = text[actual - 1, 1];
        instruct = "Presiona F para tomar el fragmento del diario";
        
    }

    void CleanDiary()
    {
        into = false;
        read = false;
        background.gameObject.SetActive(false);
        content = "";
        instruct = "";
        title = "";
    }

    private bool LoadText()
    {

        string line;
        int lines = 0;
        int total = 0;
        StringReader reader = new StringReader(archive.text);
        using(reader)
        {
             do
             {
                line = reader.ReadLine();
                if(line != null)
                {
                    if (line.Equals(""))
                    {
                        lines = 0;
                    }
                    else
                    {
                        lines++;
                        if (lines == 1)
                        {
                            text[total, 0]=line;
                            total++;
                        }
                        else
                        {
                            text[total - 1, 1] = line;
                        }
                    }
                }
            }while(line != null);
            reader.Close();
            return true;
        } 
    }

	// Use this for initialization
	void Start () {
        LoadText();
        //System.IO.File.ReadAllText("DIARIOS.txt");
	}
	
	// Update is called once per frame
	void Update () {
        if (into)
        {
			if (Input.GetKeyUp("f") || (Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(0).tapCount > 1 ) )
			{
				if (read)
				{
					CleanDiary();
					diaries[actual - 1].gameObject.SetActive(false);
					gameObject.SendMessage("AddDiarie");
					Variables.diaries[actual - 1] = true;
					Variables.redes = Variables.redes + 10;
				}
				else
				{
					read = true;
					ReadDiary();
				}
			}
            instructObj.text = instruct;
            titleObj.text = title;
            contentObj.text = content;

        }

		if (Input.GetKeyDown(KeyCode.P))
		{
			if (diariesMenu.activeInHierarchy == true)
				diariesMenu.SetActive(false);
			else {
				diariesMenu.SetActive(true);
				if(diariesPanel.activeInHierarchy == true)
					diariesPanel.SendMessage("Restart");
			}
		}
	}
}
