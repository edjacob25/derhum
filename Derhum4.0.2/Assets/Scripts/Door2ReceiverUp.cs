﻿using UnityEngine;
using System.Collections;

public class Door2ReceiverUp : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c){
		
		if (c.gameObject.tag == "Player") 
		{
			this.transform.parent.gameObject.SendMessage("Up");
			Destroy(gameObject);
		}
		
	}

}
