﻿using UnityEngine;
using System.Collections;

public class StayPolice : MonoBehaviour {

	private float y;
	private float x;
	private float z;
	
	// Use this for initialization
	void Start () {
		
		y = this.transform.position.y;

	}
	
	// Update is called once per frame
	void Update () {
		x = this.transform.position.x;
		z = this.transform.position.z;
		transform.position = new Vector3(x,y,z);
	}
}
