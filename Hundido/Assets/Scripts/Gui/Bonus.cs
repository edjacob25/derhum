﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour {

	public GameObject[] tables;

	void OnClick()
	{
		if (Variables.heart > 0 && Variables.errors > 0)
		{
			Variables.heart--;
			Variables.errors--;
			tables[Variables.errors].SetActive(true);
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
