﻿using UnityEngine;
using System.Collections;

public class Preguntas1 : MonoBehaviour {

	
	GameObject B1;
	GameObject B2;
	GameObject B3;
	GameObject B4;
	GameObject botonReinicio;
	GameObject botonMenu;
	GameObject botonContinuar;
	GameObject main;


	GameObject etiqueta;
	GameObject resultado;
	GameObject respuestaCorrecta;
	GameObject presionaA;
	GameObject correctaLabel;
	GameObject incorrectaLabel;
	GameObject fondo;
	
	bool continuar = false;
	
	string boton1 = "";
	string boton2 = "";
	string boton3 = "";
	string boton4 = "";
	
	
	int respuestaCorr = 0;
	int respuestaIncorrecta = 0;
	
	
	string labelPregunta;
	
	int respuesta = 0;
	
	int respuestaRecibida;
	
	int pregunta1;
	int pregunta2;
	int pregunta3;
	
	bool asignar = false;
	public int numeroPreguntas =0;
	public int numeroRespuestasCorrectas = 0;
	
	
	bool saberSiYaPuedePonerSpaceBar=false;
	bool saberSiYadieronRespuesta = false;


	
	// Use this for initialization
	void Start () {
		B1=GameObject.Find("Button1");
		B2=GameObject.Find("Button2");
		B3=GameObject.Find("Button3");
		B4=GameObject.Find("Button4");
		botonReinicio=GameObject.Find("restart");
		botonMenu = GameObject.Find("mainMenu");
		botonContinuar = GameObject.Find("continue");
		fondo=GameObject.Find("fondo");
		main = GameObject.Find("luz");
		//pregunta = Random.Range (1, 6);
		
		etiqueta = GameObject.Find("pregunta");
		resultado = GameObject.Find("labelResultado");
		respuestaCorrecta = GameObject.Find("respuestaCorrecta");
		presionaA = GameObject.Find("presionaA");
		correctaLabel=GameObject.Find("correctasLabel");
		incorrectaLabel=GameObject.Find("IncorrectasLabel");
		
		//botonMenubotonMenu.gameObject.SetActive (false);
		botonReinicio.gameObject.SetActive (false);
		botonContinuar.gameObject.SetActive (false);
		botonMenu.gameObject.SetActive (false);


		B1.gameObject.SetActive (false);
		B2.gameObject.SetActive (false);
		B3.gameObject.SetActive (false);
		B4.gameObject.SetActive (false);
		fondo.gameObject.SetActive (false);
		resultado.GetComponentInChildren<UILabel> ().text = "";
		//pregunta.GetComponentInChildren<UILabel> ().text = "";
		 respuestaCorrecta.GetComponentInChildren<UILabel> ().text = "";
		 presionaA.GetComponentInChildren<UILabel> ().text = "Presiona Barra de Espacio Para iniciar";
		 etiqueta.GetComponentInChildren<UILabel> ().text = "";
		//etiqueta.GetComponentInChildren<UILabel> ().text = "PSSSS!!";
	}
	// Update is called once per frame
	void Update () {
		
		
		//correctaLabel.GetComponentInChildren<UILabel> ().text = "Correctas : " +  respuestaCorr;
		//incorrectaLabel.GetComponentInChildren<UILabel> ().text = "Incorrectas : " + respuestaIncorrecta;
		
		
		/*if (saberSiYaPuedePonerSpaceBar == true) {
			
			
			botonContinuar.gameObject.SetActive (false);
			
			/////////
			///if(Input.GetKeyDown("a"))
			if(continuar == true)
			{
				
				saberSiYaPuedePonerSpaceBar = false;
				asignar = true;
				resultado.GetComponentInChildren<UILabel> ().text ="";
				respuestaCorrecta.GetComponentInChildren<UILabel> ().text = "";
				saberSiYadieronRespuesta = true;
				presionaA.GetComponentInChildren<UILabel> ().text = "";
				continuar = false;
				botonContinuar.gameObject.SetActive (false);
			}
			
		}
		*/
		if (asignar == true)
		{

		    //B1.GameObject.activeInHierarchy = false;
			//gameObject.

				pregunta1 = Random.Range (1, 6);
				QuestionAsignement(pregunta1);
				//numeroPreguntas ++;
		
			B1.GetComponentInChildren<UILabel> ().text = boton1;
			B2.GetComponentInChildren<UILabel> ().text = boton2;
			B3.GetComponentInChildren<UILabel> ().text = boton3;
			B4.GetComponentInChildren<UILabel> ().text = boton4;
			etiqueta.GetComponentInChildren<UILabel> ().text =labelPregunta;
			respuestaCorrecta.GetComponentInChildren<UILabel> ().text = "";
			
			asignar = false;
				//numeroPreguntas ++;
				
			}
			
			
		/*if(saberSiYadieronRespuesta = true)
				{
					resultado.GetComponentInChildren<UILabel> ().text = "VICTORIA";
					etiqueta.GetComponentInChildren<UILabel> ().text = "ERES UN GANADOR";
					respuestaCorrecta.GetComponentInChildren<UILabel> ().text = "TU VICTORIA ES ABOSULTA";
					
					
					B1.gameObject.SetActive (false);
					B2.gameObject.SetActive (false);
					B3.gameObject.SetActive (false);
					B4.gameObject.SetActive (false);
					botonReinicio.gameObject.SetActive (true);
					botonMenu.gameObject.SetActive (true);
					botonContinuar.gameObject.SetActive (false);
				}
				else
				{
					resultado.GetComponentInChildren<UILabel> ().text = "DERROTADO";
					etiqueta.GetComponentInChildren<UILabel> ().text = "OJALA TE HUBIERAN ABORTADO";
					respuestaCorrecta.GetComponentInChildren<UILabel> ().text = "MUERETE";
					
					B1.gameObject.SetActive (false);
					B2.gameObject.SetActive (false);
					B3.gameObject.SetActive (false);
					B4.gameObject.SetActive (false);
					botonReinicio.gameObject.SetActive (true);
					botonMenu.gameObject.SetActive (true);
					botonContinuar.gameObject.SetActive (false);
				}				
				*/
		}
	
	void saberSiContinua()
	{
		
		continuar = true;
	}

	void continueGame()
	{

		botonReinicio.gameObject.SetActive (true);
		botonMenu.gameObject.SetActive (true);
		B1.gameObject.SetActive (false);
		B2.gameObject.SetActive (false);
		B3.gameObject.SetActive (false);
		B4.gameObject.SetActive (false);
		botonContinuar.gameObject.SetActive (false);

		//botonContinuar.GetComponentInChildren<UILabel> ().text = "";

		botonReinicio.GetComponentInChildren<UILabel> ().text = "REINICIO";
		botonMenu.GetComponentInChildren<UILabel> ().text = "MAIN MAP";

		resultado.GetComponentInChildren<UILabel> ().text = "";
		respuestaCorrecta.GetComponentInChildren<UILabel> ().text = "";
		presionaA.GetComponentInChildren<UILabel> ().text = "";
		etiqueta.GetComponentInChildren<UILabel> ().text = "";

	}
	
	void recieveAnswer(int r)
	{
		presionaA.GetComponentInChildren<UILabel> ().text ="";

		if (saberSiYadieronRespuesta == true) {
			saberSiYadieronRespuesta = false;
			main.SendMessage("cambiarLabel");
	
			if (r == respuesta)
			{
			    botonContinuar.gameObject.SetActive (true);
				resultado.GetComponentInChildren<UILabel> ().text = "!!CORRECTO!!";
				presionaA.GetComponentInChildren<UILabel> ().text ="Derechitos Duplicados";
				saberSiYaPuedePonerSpaceBar = true;
			} 
			else 
			{
			    botonContinuar.gameObject.SetActive (true);
				resultado.GetComponentInChildren<UILabel> ().text = "!!INCORRECTO!!";
			    presionaA.GetComponentInChildren<UILabel> ().text ="Presione el boton Continuar";
				saberSiYaPuedePonerSpaceBar = true;

				if (respuesta == 1) {
					respuestaCorrecta.GetComponentInChildren<UILabel> ().text = boton1;
				}
				if (respuesta == 2) {
					respuestaCorrecta.GetComponentInChildren<UILabel> ().text = boton2;
				}
				if (respuesta == 3) {
					respuestaCorrecta.GetComponentInChildren<UILabel> ().text = boton3;
				}
				if (respuesta == 4) {
					respuestaCorrecta.GetComponentInChildren<UILabel> ().text = boton4;
				}
				
			}
			
			saberSiYaPuedePonerSpaceBar = true;
			saberSiYadieronRespuesta = false;
			
		}
	}


	void initQuestion()
	{


		asignar = true;
		saberSiYadieronRespuesta = true;

		B1.gameObject.SetActive (true);
		B2.gameObject.SetActive (true);
		B3.gameObject.SetActive (true);
		B4.gameObject.SetActive (true);
		fondo.gameObject.SetActive (true);


		}
	
	void reiniciar()
	{
		respuestaCorr = 0;
		respuestaIncorrecta = 0;
		numeroPreguntas = 0;
		numeroRespuestasCorrectas = 0;
		
		pregunta1 = 0;
		pregunta2 = 0;
		pregunta3 = 0;
		
		asignar = true;
		
		resultado.GetComponentInChildren<UILabel> ().text = "";
		etiqueta.GetComponentInChildren<UILabel> ().text = "";
		respuestaCorrecta.GetComponentInChildren<UILabel> ().text = "";
		
		B1.gameObject.SetActive (true);
		B2.gameObject.SetActive (true);
		B3.gameObject.SetActive (true);
		B4.gameObject.SetActive (true);
		botonReinicio.gameObject.SetActive (false);
		botonMenu.gameObject.SetActive (false);
		botonContinuar.gameObject.SetActive (false);
		
	}
	
	void QuestionAsignement(int r)
	{
		
		
		if (r == 1)
		{
			
			labelPregunta = "Los niñs tienen derecho a?";
			boton1 = "abuso";
			boton2 = "educacion";
			boton3 = "lujos";
			boton4 = "sufrir";
			respuesta = 2;
			
		}
		
		if (r == 2)
		{
			
			labelPregunta = "Los hompres tienen derecho a?";
			boton1 = "libertad";
			boton2 = "esclavitud";
			boton3 = "arder";
			boton4 = "sufrir";
			respuesta = 1;
			
		}
		
		if (r == 3)
		{
			
			labelPregunta = "cual es un derecho humano?";
			boton1 = "muerte";
			boton2 = "esclavitud";
			boton3 = "servidumbre";
			boton4 = "nacionalidad";
			respuesta = 4;
			
		}
		
		if (r == 4)
		{
			
			labelPregunta = "cual es un derecho de los niñs?";
			boton1 = "sufrimiento";
			boton2 = "esclavitud";
			boton3 = "Educacion";
			boton4 = "trabajar";
			respuesta = 3;
			
		}
		
		if (r == 5)
		{
			
			labelPregunta = "Slogan de Mcdonalds";
			boton1 = "Just do it";
			boton2 = "Me encanta";
			boton3 = "Die Motherfucker";
			boton4 = "Burning your family";
			respuesta = 2;
			
		}
		
	}
}

