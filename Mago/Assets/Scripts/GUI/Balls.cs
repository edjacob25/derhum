using UnityEngine;
using System.Collections;

public class Balls : MonoBehaviour {
	public GameObject prefab;
	int v = 0;
	float vel = 0.1f;
	// Use this for initialization
	void Start () {
		Vector3 lp = transform.position;
		lp.z = Variables.Magelife-3;
		transform.position = lp;
	}
	
	// Update is called once per frame
	void Update () {
		Quaternion lp = transform.rotation;
		//Debug.Log(lp.y);
		lp.y = lp.y + vel;
		if(lp.y >= 1)
			lp.y = -1;
		transform.rotation = lp;
		if(v%100 == 0)
			Instantiate(prefab,transform.position,transform.rotation);
		v++;
	}
}
