﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour {
	int a = 0;
	public GameObject panel1;
	public GameObject panel2;

	string host = "http://www.jbrr25.meximas.com/";
	// Use this for initialization
	void Start () {
		Application.ExternalCall("getName","1");
		StartCoroutine("GetQuestions");
		StartCoroutine("GetAnswers");
	}

	void setName(string name)
	{
		Variables.name = name;
		//Tname.GetComponent<UILabel>().text  = Variables.name;

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if(a == 0)
			{
				panel1.SetActive(false);
				panel2.SetActive(true);
				a++;
			}
			else
				Application.LoadLevel("Game");
		}
	}

	IEnumerator GetQuestions()
	{
		string questions;
		WWW www;
		string target = host + "getQuestions.php";
		www = new WWW (target);
		//Debug.Log("getting data");
		yield return www;

		if (www.error != null) 
			Debug.LogError (www.error);
		else
		{
			questions = www.text;
			Debug.Log(questions);
			Variables.questions = questions.Split(new char[]{'&'});
			//Debug.Log(Variables.questions[Variables.questions.Length - 2]);
		}
	}

	IEnumerator GetAnswers()
	{ 
		string answers;
		/*string[] answersPerQuestion;
		string[] temp;*/
		WWW www;
		string target = host + "getAnswers.php";
		www = new WWW (target);
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
		else
		{
			answers = www.text;
			Debug.Log(answers);
			Variables.answers = answers.Split(new char[]{'&'});
			/*answersPerQuestion = answers.Split(new char[]{'&'});
			Debug.Log(answersPerQuestion[0]);
			int i = 0;
			while(i < answersPerQuestion.Length-2)
			{
				temp = answersPerQuestion[i].Split('#');
				for(int j = 0; j< temp.Length; j++)
				{
					Debug.Log(temp[j]);

					Variables.answers[i,j] = temp[j];
				}
				i++;
			}
			Debug.Log(Variables.answers[0,0]);*/
		}
	}

}
