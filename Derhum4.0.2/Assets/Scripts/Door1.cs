﻿using UnityEngine;
using System.Collections;

public class Door1 : MonoBehaviour {

	public float speedY = 0.1f;
	public bool sw = false;

	// Use this for initialization
	void Start () {


		
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(sw == true) 
		{
			float y = this.transform.position.y;
			float x = this.transform.position.x;
			float z = this.transform.position.z;

			y += speedY;
			
			if (y > 7.5)
			{
				y = 7.5f;
			}
			
			transform.position = new Vector3(x,y,z);
		}


		if(sw == false) 
		{
			float y = this.transform.position.y;
			float x = this.transform.position.x;
			float z = this.transform.position.z;
			
			y -= speedY;
			
			if (y < 2.5)
			{
				y = 2.5f;
			}
			
			transform.position = new Vector3(x,y,z);
		}


	}


	void Up()
	{
		sw = true; 
	}

	void Down()
	{
		sw = false; 
	}
}
