﻿using UnityEngine;
using System.Collections;

public class Diaries : MonoBehaviour {
	int number;

	void Init(int place) {
		number = place;
		name=place+"";
		GetComponentInChildren<UILabel>().text = place+"";
	}

    void OnClick() {
		SendMessageUpwards("ListDiarie",number);
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
