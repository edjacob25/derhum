﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (Variables.music == false)
			Variables.music = true;
		else
		{
			Destroy(gameObject);
		}
	}

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
	// Update is called once per frame
	void Update () {
	
	}
}
