﻿using UnityEngine;
using System.Collections;

public class End : MonoBehaviour {
	GameObject[] arr = {null,null,null,null,null,null};
	public int num = 0;
	public GameObject a;
	public GameObject b;
	public GameObject c;
	public GameObject d;
	public GameObject e;
	public GameObject f;
	//public GameObject g;

	// Use this for initialization
	void Start () {
		//arr = GameObject.FindGameObjectsWithTag("Slide");
		arr[0] = a;
		arr[1] = b;
		arr[2] = c;
		arr[3] = d;
		arr[4] = e;
		arr[5] = f;
		//arr[6] = g;

		arr[0].transform.localScale = new Vector3(Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			Debug.Log(arr[num].GetComponent<UITexture>().mainTexture);
			if (num < arr.Length - 1)
			{
				arr[num].transform.localScale = new Vector3(0,0);
				num++;
				arr[num].transform.localScale = new Vector3(Screen.width, Screen.height);
			}
		}
	}
}
