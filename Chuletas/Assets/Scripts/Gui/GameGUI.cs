﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {
	UILabel score, time, level, bonus;
	PlayerStats playstats;
	GameObject player;
	float inita;
	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player");
		score = GameObject.Find("Score").GetComponent<UILabel>();
		time = GameObject.Find("Time").GetComponent<UILabel>();
		level = GameObject.Find("Level").GetComponent<UILabel>();
		bonus = GameObject.Find("Bonus").GetComponent<UILabel>();
		level.text = "Nivel: " + (Variables.level+ 1);
		inita = 60;


	}

	void AddBonus()
	{
		inita = inita + 30;
		Debug.Log("iinit - " + inita);
	}
	// Update is called once per frame
	void Update () {
		float showTime;
		float timeleft = inita;
		playstats = player.GetComponent<PlayerStats>();
		showTime = timeleft - Time.timeSinceLevelLoad;

		//Debug.Log(inita);
		time.text ="Tiempo: " + showTime.ToString("0.00");
		if (showTime <= 0)
		{
			//playstats.UpdateScore();
			Variables.winner = false;
			Application.LoadLevel("Pregunta");

		}
		score.text = "Puntos: "+ playstats.points;
		bonus.text = Variables.bonus.ToString();
	}
}
