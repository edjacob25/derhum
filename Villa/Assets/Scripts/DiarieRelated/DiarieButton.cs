﻿using UnityEngine;
using System.Collections;

public class DiarieButton : MonoBehaviour {

	public GameObject title;
	public GameObject content;
	public GameObject panel;

	void OnClick() {
		title.SetActive(false);
		content.SetActive(false);
		panel.SetActive(true);
		panel.SendMessage("Restart");
		gameObject.SetActive(false);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
