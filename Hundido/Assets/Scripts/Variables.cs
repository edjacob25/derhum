﻿using UnityEngine;
using System.Collections;

public class Variables {
	public static bool winner = false;
	public static int points = 0;
	public static int errors = 0;
	public static string name = "";
	public static string[] questions;
	public static string[] answers;
	public static int heart = 3;
}
