using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	//public GameObject destine;
	public GameObject ans;
	void OnClick()
	{
		string place = "game";
		//GameObject.Find("Balls").SetActive(true);

		if (gameObject.tag == "RightGuess")
		{
			if (Variables.level < 2)
			{
				if (Variables.winner)
					Variables.level ++;
			}
			else if (Variables.winner) {
				Variables.level = Variables.level * 2;
				place = "restart";
			}
		}
		else
		{
			if (!Variables.winner)
			{
				if(Variables.level > 0)
					Variables.level --;
			}
		}
		StartCoroutine(ShowAnswers(place));


	}
	// Use this for initialization

	IEnumerator ShowAnswers(string place)
	{
		ans.SetActive(true);

		GameObject.Find("Game").GetComponent<FillGame>().OrderAnswers();

		yield return new WaitForSeconds(2);
		ans.SetActive(false);
		//destine.SetActive(true);
		//GameObject.Find("Camera").GetComponent<Intro>().Initialize();
		GameObject.Find("Game").SetActive(false);
		Variables.height = 0;
		Application.LoadLevel(place);
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
