﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {
	GameObject player;
	public GameObject anim;

	void OnTriggerEnter(Collider c)
	{
		int neg = 0;
		if(c.name == "Floor")
		{
			Destroy(gameObject);
		}
		else if((c.name =="Player")||(c.tag == "StackedFood")||(c.tag =="Tray"))
		{
			//Debug.Log("Suena");
			Instantiate(anim);
			audio.Play();
			//Debug.Log(Variables.height);
			GameObject[] array = GameObject.FindGameObjectsWithTag("StackedFood");
			//Debug.Log(array.Length);
			if (array.Length <= 3)
			{
				neg = array.Length;
				for (int i = 0; i< array.Length; i++)
				{
					array[i].tag = "Untagged";
					FixedJoint a = array[i].GetComponent<FixedJoint>();
					a.breakTorque = 0;
					a.breakForce = 0;
					array[i].rigidbody.AddForce(Vector3.up * 15);

				}
				GameObject.FindGameObjectWithTag("Tray").name = "Tray"; 
			}
			else
			{
				neg = 3;
				array[array.Length-4].name = "Top";
				for (int i = array.Length -1 ; i> array.Length - 4; i--)
				{
					FixedJoint a = array[i].GetComponent<FixedJoint>();
					a.breakTorque = 0;
					a.breakForce = 0;
					array[i].rigidbody.AddForce(Vector3.up * 15);
				}
			}
			Variables.height = Variables.height - neg;
			player.SendMessage("Bomb",neg);
			Destroy(gameObject);
		}
	}
	

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
