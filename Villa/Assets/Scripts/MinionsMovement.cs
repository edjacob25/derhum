﻿using UnityEngine;
using System.Collections;

public class MinionsMovement : MonoBehaviour {

	public GameObject jaula;
	public GameObject explotion;

	bool move = false;
	bool shot = false;
	Vector3 initial;

	void OnTriggerEnter(Collider c)
	{
		if (c.tag == "Bullet")
		{
			shot = true;
		}
		if (c.tag == "Player")
		{
			//jaula.SendMessage("Move");
			move = true;
		}
	}

	// Use this for initialization
	void Start () {
		initial = transform.position;
	}

	IEnumerator Back()
	{
		move = false;
		yield return new WaitForSeconds(5);
		transform.position = initial;
	}

	// Update is called once per frame
	void Update () {
		if(move && !shot)
		{
			Instantiate(explotion,initial,Quaternion.identity);
			transform.position = Vector3.zero;
			StartCoroutine("Back");
		}
	}
}
