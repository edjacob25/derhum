﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Preguntas : MonoBehaviour {
	
	
	public CanvasGroup[] panelGroup;
	public int activePanel;
	int targetPanel;
	public Button b1,b2,b3,bCorrecto;
	Color colorOriginal;
	PlayerStats stats;
	// Use this for initialization
	void Start () {
		stats = GameObject.Find ("ControlUI").GetComponent<PlayerStats> ();
		for (int i = 0; i<panelGroup.Length; i++) {
			panelGroup [i].alpha = 0;
			panelGroup [i].blocksRaycasts = false;
		}
		panelGroup [activePanel].alpha = 0;
		panelGroup [activePanel].blocksRaycasts = false;
		colorOriginal = bCorrecto.image.color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void CambiarPanel(int panel)
	{
		stats.CancelaTorre ();
		panelGroup [activePanel].alpha = 0;
		panelGroup [activePanel].blocksRaycasts = false;
		panelGroup [panel].alpha = 1;
		panelGroup [panel].blocksRaycasts = true;
		activePanel = panel;
	}

	public void invokeCambio()
	{
		CambiarPanel (targetPanel);
		RegresarColores();
	}
	public void Acertar(int premio)
	{
		CambiarColores ();
		targetPanel = 1;
		stats.AumentarDinero(premio);
		stats.AumentarVida (20);
		Invoke ("invokeCambio", 3);


		//CambiarPanel (1);
	}
	public void Fallar()
	{
		targetPanel = 0;
		CambiarColores ();
		Invoke ("invokeCambio", 3);

	}

	void CambiarColores()
	{
		Color green = Color.green;
		Color red = Color.red;
		bCorrecto.image.color = green;
		b1.image.color = red;
		b2.image.color = red;
		b3.image.color = red;
		b1.enabled = false;
		b2.enabled = false;
		b3.enabled = false;
		bCorrecto.enabled = false;
	}

	void RegresarColores()
	{
		bCorrecto.image.color = colorOriginal;
		b1.image.color = colorOriginal;
		b2.image.color = colorOriginal;
		b3.image.color = colorOriginal;
		b1.enabled = true;
		b2.enabled = true;
		b3.enabled = true;
		bCorrecto.enabled = true;
	}

	public void CambiarNivel(string nivel)
	{
		Application.LoadLevel (nivel);
	}
	public void Aceptar()
	{
		panelGroup [activePanel].alpha = 0;
		panelGroup [activePanel].blocksRaycasts = false;
	}
}
