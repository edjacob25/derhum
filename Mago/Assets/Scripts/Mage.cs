using UnityEngine;
using System.Collections;

public class Mage : MonoBehaviour {

	public int life = 3;
	public GameObject full;
	public GameObject second;
	public GameObject third;

	void DestroyParticle()
	{
		GameObject[] arr = GameObject.FindGameObjectsWithTag("MageLife");
		Destroy(arr[arr.Length-1]);
	}

	public void getDamage(){
		life--;
		DestroyParticle();
		if (life == 2)
		{
			Instantiate(second, new Vector3(-9,6 - ((Variables.Magelife-1)* 2),5), Quaternion.Euler(270,0,0));
		}
		if( life == 1)
		{
			Instantiate(third, new Vector3(-9,6 - ((Variables.Magelife-1)* 2),5), Quaternion.Euler(270,0,0));
		}
	}
	// Use this for initialization
	void Start () {
		//Debug.Log("aqui");
		for (int i = 0; i < Variables.Magelife; i++)
		{
			Instantiate(full, new Vector3(-9,6-i*2,5), Quaternion.Euler(270,0,0));
		}


	}
	
	// Update is called once per frame
	void Update () {
		if(life < 1)
		{
			Variables.Magelife --;
			Application.LoadLevel("game");
		}
		if(Variables.Magelife < 1)
		{
			Application.LoadLevel("End");
		}
	}
}
