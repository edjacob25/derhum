﻿using UnityEngine;
using System.Collections;

public class Variables{

	public static int Playerlife = 4;
	public static int Magelife = 4;
	public static bool instructions = false;
	public static bool music = false;
	public static string name = "";
	public static string[] questions;
	public static string[] answers;
}
