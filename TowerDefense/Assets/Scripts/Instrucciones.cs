﻿using UnityEngine;
using System.Collections;

public class Instrucciones : MonoBehaviour {


	public CanvasGroup[] panelGroup;
	public int activePanel;
	// Use this for initialization
	void Start () {
		if (panelGroup.Length != 0) {
			for (int i = 0; i<panelGroup.Length; i++) {
				panelGroup [i].alpha = 0;
				panelGroup [i].blocksRaycasts = false;
			}
			panelGroup [activePanel].alpha = 1;
			panelGroup [activePanel].blocksRaycasts = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CambiarPanel(int panel)
	{
		panelGroup [activePanel].alpha = 0;
		panelGroup [activePanel].blocksRaycasts = false;
		panelGroup [panel].alpha = 1;
		panelGroup [panel].blocksRaycasts = true;
		activePanel = panel;
	}

	public void CambiarNivel(string nivel)
	{
		Application.LoadLevel (nivel);
	}
}


