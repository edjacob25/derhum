﻿using UnityEngine;
using System.Collections;

public class antiMinion : MonoBehaviour {


	GameObject minion1Jaula;
	GameObject minion2Jaula;
	GameObject minion3Jaula;
	GameObject minion4Jaula;
	GameObject minion5Jaula;

	GameObject minion1;
	GameObject minion2;
	GameObject minion3;
	GameObject minion4;
	GameObject minion5;
	public GameObject particle;

	GameObject mainController;
	// Use this for initialization
	void Start () {

		minion1 = GameObject.Find ("minion1");
		minion2 = GameObject.Find ("minion2");
		minion3 = GameObject.Find ("minion3");
		minion4 = GameObject.Find ("minion4");
		minion5 = GameObject.Find ("minion5");

		minion1Jaula = GameObject.Find ("jaulaMinion1");
		minion2Jaula = GameObject.Find ("jaulaMinion2");
		minion3Jaula = GameObject.Find ("jaulaMinion3");
		minion4Jaula = GameObject.Find ("jaulaMinion4");
		minion5Jaula = GameObject.Find ("jaulaMinion5");

		mainController = GameObject.Find("First Person Controller");

		minion1Jaula.gameObject.SetActive (false);
		minion2Jaula.gameObject.SetActive (false);
		minion3Jaula.gameObject.SetActive (false);
		minion4Jaula.gameObject.SetActive (false);
		minion5Jaula.gameObject.SetActive (false);
	
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void disapear1()
	{
		Instantiate(particle,minion1.transform.position,Quaternion.identity);
		minion1.gameObject.SetActive (false);
		minion1Jaula.gameObject.SetActive (false);
	}
	void disapear2()
	{
		Instantiate(particle,minion2.transform.position,Quaternion.identity);
		minion2.gameObject.SetActive (false);
		minion2Jaula.gameObject.SetActive (false);
		
	}
	void disapear3()
	{
		Instantiate(particle,minion3.transform.position,Quaternion.identity);
		minion3.gameObject.SetActive (false);
		minion3Jaula.gameObject.SetActive (false);
	}
	void disapear4()
	{
		Instantiate(particle,minion4.transform.position,Quaternion.identity);
		minion4.gameObject.SetActive (false);
		minion4Jaula.gameObject.SetActive (false);
	}
	void disapear5()
	{
		Instantiate(particle,minion5.transform.position,Quaternion.identity);
		minion5.gameObject.SetActive (false);
		minion5Jaula.gameObject.SetActive (false);
	}


	void recieveMinion(int min)
	{
		if (min == 1)
		{
			minion1Jaula.gameObject.SetActive (true);
			mainController.SendMessage("minionsCapturados",1);
			minion1Jaula.SendMessage("init");
			Invoke("disapear1",3);
		}
		if (min == 2)
		{
			minion2Jaula.gameObject.SetActive (true);
			mainController.SendMessage("minionsCapturados",2);
			minion2Jaula.SendMessage("init");
			Invoke("disapear2",3);
		}
		if (min == 3)
		{
			minion3Jaula.gameObject.SetActive (true);
			mainController.SendMessage("minionsCapturados",3);
			minion3Jaula.SendMessage("init");
			Invoke("disapear3",3);

		}
		if (min == 4)
		{
			minion4Jaula.gameObject.SetActive (true);
			mainController.SendMessage("minionsCapturados",4);
			minion4Jaula.SendMessage("init");
			Invoke("disapear4",3);
		}
		if (min == 5)
		{
			minion5Jaula.gameObject.SetActive (true);
			mainController.SendMessage("minionsCapturados",5);
			minion5Jaula.SendMessage("init");
			Invoke("disapear5",3);
		}

	}
}
