﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
	CharacterController controller;
	GameObject player;

	// Use this for initialization
	void Start () 
	{
		//player = GameObject.FindGameObjectWithTag("Enemy");
		controller = this.GetComponent<CharacterController>();
	}
	
	void BulletMove()
	{
		if (player != null) {
			Vector3 p = player.transform.position;
			p.y = this.transform.position.y;
			this.transform.LookAt (p);
		
			controller.Move (transform.TransformDirection (Vector3.forward) * 0.5f);
		} else
			Destroy (gameObject);
	}

	// Update is called once per frame
	void Update () 
	{
		BulletMove();
	}
	public void SetPlayer(GameObject play)
	{
		player = play;
	}
}
