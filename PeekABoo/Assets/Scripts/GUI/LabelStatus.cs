﻿using UnityEngine;
using System.Collections;

public class LabelStatus : MonoBehaviour {
	public GameObject points;
	string host = "http://www.jbrr25.meximas.com/";
	// Use this for initialization
	void Start () {

		if (Variables.winner){
			GetComponent<UILabel>().color = Color.green;
			GetComponent<UILabel>().text = "¡Ganador!";

		}
		else {
			GetComponent<UILabel>().color = Color.red;
			GetComponent<UILabel>().text = "¡Has perdido!";
		}
		points.GetComponent<UILabel>().text = "Felicidades "+Variables.name+" conseguiste "+Variables.money+" Derechitos, muy bien";
		StartCoroutine("setMoney");
	}

	IEnumerator setMoney()
	{
		WWW www;
		WWWForm form;
		form = new WWWForm();
		string target = host +"addMoney.php";
		form.AddField("username",Variables.name);
		form.AddField("money",Variables.money);
		www = new WWW (target,form);
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
		else
			Debug.Log(www.text);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
