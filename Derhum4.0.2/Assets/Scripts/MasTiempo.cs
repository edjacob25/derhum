﻿using UnityEngine;
using System.Collections;

public class MasTiempo : MonoBehaviour {

	public GameObject canvasTiempo;
	public GameObject jugador;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter(Collider c){
		if (c.gameObject.tag == "Player") {
			jugador.SendMessage("moreTime",1,SendMessageOptions.DontRequireReceiver);
			canvasTiempo.SetActive(true);
			Destroy(gameObject);
		}
	}
}
