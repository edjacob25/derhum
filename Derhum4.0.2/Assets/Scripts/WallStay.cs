﻿using UnityEngine;
using System.Collections;

public class WallStay : MonoBehaviour {
	
	private float y;
	private float x;
	private float z;
	
	// Use this for initialization
	void Start () {

		x = this.transform.position.x;
		z = this.transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		y = this.transform.position.y;
		transform.position = new Vector3(x,y,z);
	}
}
