﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Funciones : MonoBehaviour {

	public GameObject canvasPregunta;
	public GameObject wall;
	public GameObject contador;
	public GameObject jugador;
	//public GameObject[] mapas;
	// Use this for initialization
	void Start () {
		gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void preguntaNombre() {
		canvasPregunta.SetActive (false);
		gameObject.SetActive (true);
	}

	public void continuar() {
		gameObject.SetActive (false);
		UnityEngine.Time.timeScale = 1.0f;
	}

	public void pregunta2Correcta () {
		canvasPregunta.SetActive (false);
		gameObject.SetActive (true);
		contador.SendMessage("subirDerechitos2",1,SendMessageOptions.DontRequireReceiver);
		UnityEngine.Time.timeScale = 1.0f;
	}
	public void pregunta2Incorrecta () {
		canvasPregunta.SetActive (false);
		wall.SendMessage("Change",1, SendMessageOptions.DontRequireReceiver);
		contador.SendMessage("bajarDerechitos",1,SendMessageOptions.DontRequireReceiver);
		gameObject.SetActive (true);
		UnityEngine.Time.timeScale = 1.0f;
	}

	public void level2() {
		Application.LoadLevel ("Level 2");
		}

	public void win() {
		Application.LoadLevel (0);
	}
}
