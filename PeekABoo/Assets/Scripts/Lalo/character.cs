﻿using UnityEngine;
using System.Collections;

public class character : MonoBehaviour {

	GameObject cuadroDeDialogo;
	GameObject dialogo;
	GameObject informacion;
	GameObject nuevoItem;
	GameObject personajeDice;

	GameObject puerta1;
	GameObject puerta2;

	string dialogoString = "";
	string informacionString = "";
	string nuevoItemString = "";
	string personajeDiceString = "";

	 bool cercaDePuerta = false;
	 bool cercaDeAldeana = false;


	bool cambioPuertaDecir = false;
	bool yaAbrioLaPuerta = false;

	bool inicioDialogoConMarcela = false;
	bool segundoDialogoDeMarcela = false;
	bool finalizarDialogoMarcela = false;
	bool totalEndConversation = false;
	bool tieneLlave = false;

	int segundaConv = 0;



	bool postConversacion1 = false;
	bool postConversacionEnd = false;

	// Use this for initialization
	void Start () {

		cuadroDeDialogo=GameObject.Find("cuadrodeDialogo");
		dialogo=GameObject.Find("dialogo");
		informacion=GameObject.Find("informacion");
		nuevoItem=GameObject.Find("nuevoItem");
		personajeDice=GameObject.Find("PersonajeDice");
		puerta1 = GameObject.Find ("puertaDoble1");
		puerta2 = GameObject.Find ("puerta2");

	    cuadroDeDialogo.gameObject.SetActive (false);

	
	}
	
	// Update is called once per frame
	void Update () {




		if(yaAbrioLaPuerta == false)
		{
		if (cercaDePuerta == true)
		{ 
				if(cambioPuertaDecir == false)
				{
			       informacionString = "Presiona A para abrirLaPuerta";
				}
				else
				{
					informacionString = "Necesitas tener una llave";
				}
			if (Input.GetKeyDown ("a"))
			{
				if(tieneLlave == false)
				{
					cambioPuertaDecir = true;
				}
				else 
				{
						informacionString = "";
						yaAbrioLaPuerta = true;
						nuevoItemString = "";
					//initPuerta();
					    puerta1.SendMessage("initRotate");
						puerta2.SendMessage("initRotate");
				}
			}
		}

		}


		if (cercaDeAldeana == true && tieneLlave == true)
		{
			informacionString = "Presiona A para Hablar con Marcela";

			if(Input.GetKeyDown ("a") && segundaConv ==1)
			{
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				segundaConv = 2;
				
			}


			if(segundaConv == 0)
			{
			if(Input.GetKeyDown ("a"))
			{
			cuadroDeDialogo.gameObject.SetActive (true);
			dialogoString = "los Malechores estan en el 2ndo piso de la casa detras de mi";
			informacionString = "Presiona A para Finalizar la conversación";
			segundaConv ++;
			 
			}

		}

			if(segundaConv == 2)
			{
				segundaConv = 0;
			}


		}

		if (cercaDeAldeana == true && tieneLlave == false)
		{



			if(finalizarDialogoMarcela == true && Input.GetKeyDown ("a") )
			{
				nuevoItemString = "Has obtenido la llave de la casa, ve a la casa y ayuda a esas personas";
				cuadroDeDialogo.gameObject.SetActive (false);
				informacionString = "";
				personajeDiceString = "";
				dialogoString = "";
				tieneLlave = true;
				
			}

			if(segundoDialogoDeMarcela == true && Input.GetKeyDown ("a") )
			{
				if(finalizarDialogoMarcela == false)
				{
				dialogoString = "Acaso nos podrias ayudar? \n los Malechores estan en el 2ndo piso, han capturado a mis hermanas \n Aqui esta la llave.";
				informacionString = "Presiona A para Finalizar la conversación";
				//nuevoItemString = "Has obtenido la llave de la casa, ve a la casa y ayuda a esas";
				finalizarDialogoMarcela = true;
				}
			}





			

			if(inicioDialogoConMarcela == false)
			informacionString = "Presiona A para Hablar con Marcela";
			else
			{
				if(segundoDialogoDeMarcela == false)
				{
				cuadroDeDialogo.gameObject.SetActive (true);
				informacionString = "Presiona A para Continuar la conversación";
				dialogoString = "!Auxilio,auxilio! Unos Malechores entraron en nuestra Casa, \n !y no hay nadie que nos ayude";
				personajeDiceString = "Aldeana Marcela";
				segundoDialogoDeMarcela = true;
				}
			}




			if(Input.GetKeyDown ("a"))
				inicioDialogoConMarcela = true;

		}

		dialogo.GetComponentInChildren<UILabel> ().text = dialogoString;
		informacion.GetComponentInChildren<UILabel> ().text = informacionString;
		nuevoItem.GetComponentInChildren<UILabel> ().text = nuevoItemString;
		personajeDice.GetComponentInChildren<UILabel> ().text = personajeDiceString;


	
	}
	void OnTriggerExit(Collider other)
	{
		if (other.name == "AldeanaMarcela")
		{
			cercaDeAldeana = false;


			dialogoString = "";
		    informacionString = "";
			//nuevoItemString = "";
		    personajeDiceString = "";


			inicioDialogoConMarcela = false;
		    segundoDialogoDeMarcela = false;
		    finalizarDialogoMarcela = false;
			totalEndConversation = false;
		    

		}
		if (other.name == "puertaDoble1")
		{
			cercaDePuerta = false;
			informacionString = "";

		}

		}
	void OnTriggerEnter(Collider other)
	{
		//cercaDeAldeana = false;
		//cercaDePuerta = false;
		if (other.name == "cuadroDetector")
		{

			//////////jb aqui cargas la escena en el momento en que toca este colider inicia la escena nueva
			/// //////////////////////////////////////////////////////////////////////
		
		}


		if (other.name == "AldeanaMarcela") 
		{
						//informacionString = "Presiona A para Hablar con Marcela";
			cercaDeAldeana = true;
		}
		else
		{
			informacionString = "";
			cercaDeAldeana = false;

		}


		if (other.name == "puertaDoble1") 
		{
			//informacionString = "Presiona A para abrirLaPuerta";
			cercaDePuerta = true;
		}
		else
		{
			informacionString = "";
			//informacionString = "";
			cercaDePuerta = false;
			cambioPuertaDecir = false;
		}
		
	}
}
