﻿using UnityEngine;
using System.Collections;

public class RestartButtons : MonoBehaviour {
	void OnClick () {
		if (gameObject.name == "Main") {
			Application.ExternalCall("sendStatus","2");
			Application.ExternalCall("changeScene","0");
		}
		else {
			Variables.errors = 0;
			Variables.points = 0;
			Variables.heart = 3;
			Application.LoadLevel("game");
		}
	}

	// Use this for initialization
	void Start () {
		Application.ExternalCall("sendStatus","2");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
