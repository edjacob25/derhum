﻿using UnityEngine;
using System.Collections;

public class movePlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	void Move()
	{
		float move;
		Vector3 lp = transform.position;
		move = Input.GetAxis("Horizontal")*0.2f;

		if ((transform.position.x >= 9)&&(move < 0)){
			lp.x = transform.position.x + move;
		}
		else if ((transform.position.x <= -9)&&(move > 0)){
			lp.x = transform.position.x + move;
		}
		else if ((transform.position.x >= -9)&&(transform.position.x <= 9)){
			lp.x = transform.position.x + move;
		}
		transform.position = lp;
	}

	void OnTriggerEnter(Collider c)
	{
		if(c.name == "Obstacle"){
			GameObject[] array = GameObject.FindGameObjectsWithTag("StackedFood");
			Debug.Log("here");
			if (array.Length <= 3)
			{
				for (int i = 0; i< array.Length; i++)
				{
					FixedJoint a = array[i].GetComponent<FixedJoint>();
					a.breakTorque = 0;
					a.breakForce = 0;
				}
				GameObject.FindGameObjectWithTag("Tray").name = "Tray"; 
			}
			else
			{
				array[array.Length-4].name = "Top";
				for (int i = array.Length -1 ; i> array.Length - 4; i--)
				{
					FixedJoint a = array[i].GetComponent<FixedJoint>();
					a.breakTorque = 0;
					a.breakForce = 0;
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
		Move ();
	}
}
