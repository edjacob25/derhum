﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

	public int vida;
	public Text tVida;
	public int dinero;
	public Text tDinero;
	public int ronda;
	public Text tRonda;

	int dineroResta;
	
	GUIStyle infoBotones = new GUIStyle();

	float tiempoIncial, tiempoActual;

	bool movimiento;
	bool torre;
	bool vender;
	
	Rect pos;
	int numBoton;
	int numTorre;

	Ray myRay;
	RaycastHit hit;
	public LayerMask layer;
	public int segundosPoro = 1;

	public GameObject particlesCreate;
	public GameObject particlesDestroy;

	public GameObject torreObj;
	public GameObject torreObj2;
	public GameObject torreObj3;
	public GameObject torreObj4;
	GameObject torreObjClone;
	GameObject hitCopy;

	public GameObject bTorre1,bTorre2,bTorre3,bTorre4;
	Button bScript1,bScript2,bScript3,bScript4;
	public Text descTorre, descSniper, descTurret, descbazooka;

	public Texture sTorre1,sSniper,sTurret,sBazooka;
	public Texture circle;

	public GameObject tower;
	vidaCollider towerControl;
	GameObject camara;
	float proporcion;

	Preguntas paneles;
	// Use this for initialization
	void Start () {
		bScript1 = bTorre1.GetComponent<Button> ();
		bScript2 = bTorre2.GetComponent<Button> ();
		bScript3 = bTorre3.GetComponent<Button> ();
		bScript4 = bTorre4.GetComponent<Button> ();
		infoBotones.normal.background = Texture2D.whiteTexture;
		infoBotones.normal.textColor = Color.black;
		infoBotones.alignment = TextAnchor.MiddleCenter;
		movimiento = false;
		torre = false;
		vender = false;
		camara = GameObject.Find ("Main Camera");
		proporcion = 29f / camara.transform.position.y;
		paneles = GameObject.Find("ControlPanel").GetComponent<Preguntas>();
		towerControl = tower.GetComponent<vidaCollider> ();
		HideInfo ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ronda == 5) {
			paneles.CambiarPanel (3);
		}
		if(vida <=0)
			Application.LoadLevel ("Perder");
		if (ronda == 1) {
			bScript3.interactable = false;
			bScript4.interactable = false; 
		} else {
			bScript3.interactable = true;
			bScript4.interactable = true;
		}

		if (movimiento) {
			tiempoActual = Time.time;
			if(tiempoActual - tiempoIncial > segundosPoro)
			{
				AumentarDinero(1);
				tiempoIncial = tiempoActual;
			}
		}
		if (torre||vender) {
			if(Input.GetMouseButtonDown(0))
			{
				myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				if(Physics.Raycast(myRay, out hit, 10000, layer.value  ))
				{
					if(torre)
					{
						if(hit.collider.tag == "towerzone")
						{
							switch(numTorre)
							{
							case 1:
								torreObjClone = Instantiate(torreObj,new Vector3(hit.collider.transform.position.x,hit.collider.transform.position.y+0.5f,hit.collider.transform.position.z), Quaternion.identity) as GameObject;
								break;
							case 2:
								torreObjClone = Instantiate(torreObj2,new Vector3(hit.collider.transform.position.x,hit.collider.transform.position.y+0.5f,hit.collider.transform.position.z), Quaternion.identity) as GameObject;
								break;
							case 3:
								torreObjClone = Instantiate(torreObj3,new Vector3(hit.collider.transform.position.x,hit.collider.transform.position.y+0.5f,hit.collider.transform.position.z), Quaternion.identity) as GameObject;
								break;
							case 4:
								torreObjClone = Instantiate(torreObj4,new Vector3(hit.collider.transform.position.x,hit.collider.transform.position.y+0.5f,hit.collider.transform.position.z), Quaternion.identity) as GameObject;
								break;

							}
							hitCopy = Instantiate(particlesCreate, new Vector3(hit.collider.transform.position.x,hit.collider.transform.position.y+0.5f,hit.collider.transform.position.z),Quaternion.AngleAxis(90,Vector3.right)) as GameObject;
							dinero -= dineroResta;
							torre = false;
						}
					}
					else
					{
						if(hit.collider.tag == "tower")
						{
							//Destroy(hit.collider.transform.parent.gameObject);
							Destroy(hit.collider.transform.gameObject);
							dinero +=100;
							vender = false;
						}
						if(hit.collider.tag == "tower2")
						{
							//Destroy(hit.collider.transform.parent.gameObject);
							Destroy(hit.collider.transform.gameObject);
							dinero +=125;
							vender = false;
						}
						if(hit.collider.tag == "tower3")
						{
							//Destroy(hit.collider.transform.parent.gameObject);
							Destroy(hit.collider.transform.gameObject);
							dinero +=150;
							vender = false;
						}
						if(hit.collider.tag == "tower4")
						{
							//Destroy(hit.collider.transform.parent.gameObject);
							Destroy(hit.collider.transform.gameObject);
							dinero +=175;
							vender = false;
						}

						hitCopy = Instantiate(particlesDestroy, new Vector3(hit.collider.transform.position.x,hit.collider.transform.position.y+0.5f,hit.collider.transform.position.z),Quaternion.AngleAxis(90,Vector3.right)) as GameObject;
					}

				}

			}
		}
	}

	void OnGUI()
	{

		if (torre) {
			switch(numTorre)
			{

			case 1:
				GUI.DrawTexture(new Rect(Input.mousePosition.x-47f*proporcion,620f-90f*proporcion-Input.mousePosition.y,90f*proporcion,90f*proporcion),sTorre1);
				GUI.DrawTexture(new Rect(Input.mousePosition.x-110f*proporcion,600f-225f*proporcion/2-Input.mousePosition.y,225f*proporcion,225f*proporcion),circle);
				break;
			case 2:
				GUI.DrawTexture(new Rect(Input.mousePosition.x-35f*proporcion,620f-70f*proporcion-Input.mousePosition.y,70f*proporcion,70f*proporcion),sSniper);
				GUI.DrawTexture(new Rect(Input.mousePosition.x-220f*proporcion,600-440*proporcion/2-Input.mousePosition.y,440f*proporcion,440f*proporcion),circle);
				break;
			case 3:
				GUI.DrawTexture(new Rect(Input.mousePosition.x-27f*proporcion,620f-70f*proporcion-Input.mousePosition.y,55f*proporcion,70f*proporcion),sTurret);
				GUI.DrawTexture(new Rect(Input.mousePosition.x-125f*proporcion,600f-250f*proporcion/2-Input.mousePosition.y,250f*proporcion,250f*proporcion),circle);
				break;
			case 4:
				GUI.DrawTexture(new Rect(Input.mousePosition.x-42f*proporcion,620f-85*proporcion-Input.mousePosition.y,85f*proporcion,85f*proporcion),sBazooka);
				GUI.DrawTexture(new Rect(Input.mousePosition.x-125f*proporcion,600f-250*proporcion/2-Input.mousePosition.y,250f*proporcion,250f*proporcion),circle);
				break;
			}
		}
		tVida.text = "Vida:        "+vida;
		tDinero.text = "Derechitos: " + dinero;
		tRonda.text = "Ronda: " + ronda+"/4";
	}

	public void IniciarJuego()
	{
		tiempoIncial = Time.time;
		movimiento = true;
	}

	public void TerminarJuego()
	{
		movimiento = false;
	}

	public void AumentarDinero(int cantidad)
	{
		dinero = dinero + cantidad;
	}

	public void AumentarVida(int cantidad)
	{
		if (vida + cantidad > 200)
			vida = 200;
		else
			vida += cantidad;
		towerControl.ChangeColor (vida / 200f);
	}

	public void PermiteTorre(int costo)
	{
		if (costo <= dinero) {
			torre = true;
			vender = false;
			dineroResta = costo;
			numTorre = 1;
		}
	}
	public void PermiteTorre2(int costo)
	{
		if (costo <= dinero) {
			torre = true;
			vender = false;
			dineroResta = costo;
			numTorre = 2;
		}
	}
	public void PermiteTorre3(int costo)
	{
		if (costo <= dinero) {
			torre = true;
			vender = false;
			dineroResta = costo;
			numTorre = 3;
		}
	}

	public void PermiteTorre4(int costo)
	{
		if (costo <= dinero) {
			torre = true;
			vender = false;
			dineroResta = costo;
			numTorre = 4;
		}
	}

	public void PermiteVender()
	{
		vender = true;
		torre = false;
	}

	public void CancelaTorre()
	{
		torre = false;
		vender = false;
	}

	public void ShowInfo(int num)
	{
		switch (num) {
		case 1:
			descTorre.enabled = true;
			break;
		case 2:
			descSniper.enabled = true;
			break;
		case 3:
			descTurret.enabled = true;
			break;
		case 4:
			descbazooka.enabled = true;
			break;
		}

	}

	public void HideInfo()
	{
		descTorre.enabled = false;
		descSniper.enabled = false;
		descTurret.enabled = false;
		descbazooka.enabled = false;
	}

	public void CambiaRonda(int num)
	{
		ronda = num;
	}

	public void SiguienteNivel()
	{
		Application.LoadLevel (Application.loadedLevel +1);
	}
}
