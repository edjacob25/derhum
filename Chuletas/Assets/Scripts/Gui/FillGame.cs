﻿using UnityEngine;
using System.Collections;

public class FillGame : MonoBehaviour {
	public GameObject buttonA;
	public GameObject buttonB;
	public GameObject buttonC;
	public GameObject buttonD;
	public GameObject question;
	public GameObject status;
	public GameObject instructions;
	// Use this for initialization
	
	/* string[] questions = {
		"Usar los bienes de uso común como calles, semáforos, banquetas, parques y jardines es un derecho:",
		"Convivir de manera respetuosa con todas las personas es un derecho:",
		"La ley federal de los trabajadores al servicio del estado protege a:",
		"Fomentar la cultura democrática es un:",
		"Respetar la integridad física y emocional, la salud y la sexualidad de los demás es un derecho"
	};
	
	string[,] answers = {
		{"Social","De la mujer","Familiar","De los niños"},
		{"Social","De la mujer","Familiar","De los niños"},
		{"Trabajadores del estado",	"Mujeres", "Niños y jóvenes"," Jugadores de Futbol"	},
		{"Deber", "Derecho", "Juego", "Pensamiento"},
		{"Social","De la mujer","Familiar","De los niños"}
	}; */
	
	GameObject[] buttons = {null,null,null,null};

	void putQuestions(int numQuestion){
		int[] used = {-1,-1,-1,-1};
		bool diff = true;
		int pos = 0;
		string[] ans = Variables.answers[numQuestion].Split(new char[]{'#'});
		for (int i = 0; i<4; i++)
		{
			do
			{
				pos = Mathf.FloorToInt(Random.value* 3.99f);
				if (used[pos] == -1)
				{
					diff = true;
					used[pos] = 1;
				}
				else
					diff = false;
			}while (diff == false);
	
			buttons[pos].GetComponentInChildren<UILabel>().text = ans[i];
			if (i == 0)
			{
				buttons[pos].tag = "RightGuess";
			}
		}
		question.GetComponent<UILabel>().text = Variables.questions[numQuestion];
		
	}

	void Start () {
		//Debug.Log(i);
		buttons[0] = buttonA;
		buttons[1] = buttonB;
		buttons[2] = buttonC;
		buttons[3] = buttonD;
		int qs = Mathf.FloorToInt(Random.value*(Variables.questions.Length-1.01f));
		putQuestions(qs);
		Debug.Log(qs);
		/*buttonA.GetComponentInChildren<UILabel>().text =  answers[i,0];
		buttonA.tag = "RightGuess";
		buttonB.GetComponentInChildren<UILabel>().text =  answers[i,1];
		buttonC.GetComponentInChildren<UILabel>().text =  answers[i,2];
		buttonD.GetComponentInChildren<UILabel>().text =  answers[i,3];*/
		string statusText, ins;
		UILabel sLabel = status.GetComponent<UILabel>();
		UILabel iLabel = instructions.GetComponent<UILabel>();
		//Debug.Log(Variables.level);
		if (Variables.level < 2)
		{
			if (Variables.winner == true)
			{
				statusText = "Ganador";
				sLabel.color = Color.green;
				ins = "Si deseas ir al siguiente nivel, responde adecuadamente la siguiente pregunta";
			}
			else
			{
				statusText = "Has perdido";
				sLabel.color = Color.red;
				ins = "Si deseas mantener el nivel, responde adecuadamente la siguiente pregunta";
			}
		}
		else
		{
			if (Variables.winner == true)
			{
				statusText = "Ganador";
				sLabel.color = Color.green;
				ins = "Tienes "+Variables.level*100+ " derechitos, si deseas duplicar tus derechitos, responde adecuadamente la siguiente pregunta";
			}
			else
			{
				statusText = "Has perdido";
				sLabel.color = Color.red;
				ins = "Si deseas mantener el nivel, responde adecuadamente la siguiente pregunta";
			}

		}
		sLabel.text = statusText;
		iLabel.text = ins;
	}

	public void Initialize()
	{
		for(int i = 0; i<4; i++)
		{
			buttons[i].tag = "Untagged";
		}
		int qs = Mathf.FloorToInt(Random.value*(Variables.questions.Length-1.01f));
		putQuestions(qs);
	}
	
	public void OrderAnswers()
	{
		GameObject[] arr = GameObject.FindGameObjectsWithTag("Answers");
		int pos = 0;
		for(int i = 0; i < 4; i++)
		{

			Vector3 lp = buttons[i].transform.localPosition;
			lp.y = lp.y -50;
			if (buttons[i].tag =="RightGuess"){
				pos = i;
			}
			arr[i].transform.localPosition = lp;

		}
		if(pos != 3)
		{
			Vector3 temp = arr[pos].transform.localPosition;
			arr[pos].transform.localPosition = arr[3].transform.localPosition;
			arr[3].transform.localPosition = temp;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
