﻿using UnityEngine;
using System.Collections;

public class Statistics : MonoBehaviour {
	float timeleft = 0.1f;
	public GameObject time;
	public GameObject points;
	public GameObject heart;
	float targetTime;
	// Use this for initialization
	void Start () {
		SetTime();
	}

	void SetTime()
	{
		targetTime = Time.time + 30;
		timeleft = 0.01f;
	}
	
	// Update is called once per frame
	void Update () {
		if (timeleft > 0)
			timeleft = targetTime - Time.time;
		else 
			timeleft = 0;
		time.GetComponent<UILabel>().text = "Tiempo: " + timeleft.ToString("00");;
		points.GetComponent<UILabel>().text = "Puntos: " + Variables.points*20+"/200";
		heart.GetComponent<UILabel>().text = Variables.heart+"";
	}
}
