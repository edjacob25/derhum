﻿using UnityEngine;
using System.Collections;

public class Door2ReceiverDown : MonoBehaviour {

	private float y;
	private float x;
	private float z;
	
	// Use this for initialization
	void Start () {
		
		y = this.transform.position.y;
		x = this.transform.position.x;
		z = this.transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(x,y,z);
	}
	
	void OnTriggerEnter(Collider c){
		
		if (c.gameObject.tag == "Player") 
		{
			this.transform.parent.gameObject.SendMessage("Down");
			Destroy(gameObject);
		}
		
	}
}
