﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {
	GameObject player;
	GameObject savior;
	bool stacked = false;
	// Use this for initialization
	void OnTriggerEnter(Collider c)
	{
		if (((c.name =="Top")||(c.name == "Tray"))&&(gameObject.tag !="StackedFood"))
		{
			//Debug.Log("choque");
			/*gameObject.AddComponent<HingeJoint>();
			hingeJoint.breakForce = 200;
			hingeJoint.breakTorque = 200;
			hingeJoint.connectedBody = c.rigidbody;*/
			audio.Play();
			player.SendMessage("AddPoint");
			if(Variables.height < 25){
				Variables.height++;
				Vector3 lp = gameObject.transform.position;
				lp = c.transform.position;
				lp.y += .5f;
				gameObject.transform.position = lp;
				gameObject.transform.rotation = Quaternion.identity;

				FixedJoint a = gameObject.AddComponent("FixedJoint") as FixedJoint;
				a.connectedBody = c.gameObject.rigidbody;
				collider.isTrigger = true;
				tag = "StackedFood";
				name = "Top";
				c.name = "Medium";
				stacked = true;
				//Destroy(savior);
			}
			else
			{
				Destroy(gameObject);
			}

		}
		//gameObject.collider.isTrigger = true;
		if (c.name == "Floor")
		{
			//Debug.Log("piso");
			//Destroy(gameObject);
			savior.SendMessage("Save");
			if(tag != "StackedFood")
				player.SendMessage("Fallen");
		}

		if ((c.tag == "Savior")&&(gameObject.tag !="StackedFood"))
		{
			Vector3 lp = gameObject.transform.position;
			lp = c.transform.position;
			lp.y += .5f;
			gameObject.transform.position = lp;
			gameObject.transform.rotation = Quaternion.identity;
			
			FixedJoint a = gameObject.AddComponent("FixedJoint") as FixedJoint;
			a.connectedBody = c.gameObject.rigidbody;
		}
	}

	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		GameObject[] arr = GameObject.FindGameObjectsWithTag("Savior");
		savior = arr[arr.Length -1];
		//Debug.Log("Soy " + savior.name);

	}

	// Update is called once per frame
	void Update () {
		//if (!stacked)
		//{
			savior.SendMessage("PosX",transform.position.x);
			if (transform.position.y < -10)
			{
				Destroy(savior);
				Destroy(gameObject);
			}
		//}
	}
	
}
