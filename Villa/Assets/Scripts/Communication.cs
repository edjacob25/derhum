﻿using UnityEngine;
using System.Collections;
using System;
public class Communication : MonoBehaviour {
	string target;
	WWW www;
	WWWForm form;
	string host = "http://lad.tol.itesm.mx/derhum/";
	Action callback;
	
	public IEnumerator Authorization(string username, string password, Action cb)
	{
		callback = cb;
		string auth="";
		target = host + "login.php";
		form = new WWWForm();
		form.AddField("name",username);
		form.AddField("pass",password);
		
		Debug.Log("Problem here");
		www = new WWW (target,form);
		
		//while(!www.isDone)
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
		else 
			auth = www.text;
		
		Debug.Log(auth);
		
		if (auth == "Authorized"){
			//Application.ExternalCall("loadMinigames", username);
			Variables.name = username;
			Variables.result = auth;
		}
		else
			Variables.result = "Failed";
		callback();
	}
	
	void SetScore(int game, int score)
	{
		target = "http://localhost/Derhum/login.php";
		form = new WWWForm();
		form.AddField("game",game);
		form.AddField("score",score);
		www = new WWW(target,form);
	}
	
	string GetScore(int game)
	{
		target = "http://localhost/Derhum/login.php";
		form = new WWWForm();
		form.AddField("game",game);
		www = new WWW(target,form);
		return www.text;
	}
	
	string GetPlayerData(string player)
	{
		target = "http://localhost/Derhum/login.php";
		form = new WWWForm();
		form.AddField("player",player);
		www = new WWW(target,form);
		return www.text;
	}
	
	void SetUsername(string username){
		Variables.name = username;
	}
	
	void Awake()
	{
		//DontDestroyOnLoad(transform.gameObject);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}