using UnityEngine;
using System.Collections;

public class ChangeZone : MonoBehaviour {

	public string dir;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider c)
	{

		if (c.gameObject.tag == "Enemy" || c.gameObject.tag == "EnemyBoss") {
			c.SendMessage ("CambiarDireccion", dir, SendMessageOptions.DontRequireReceiver);
		}
	}
}
