﻿using UnityEngine;
using System.Collections;

public class Winner : MonoBehaviour {

	void OnTriggerEnter(Collider c)
	{
		if (c.name == "Top")
		{
			GameObject.Find("Player").SendMessage("UpdateScore");
			Variables.winner = true;
			Application.LoadLevel("Pregunta");
		}
		//Debug.Log(c.name + " me toco");
	}
	// Use this for initialization
	void Start () {
		Vector3 lp = transform.position;
		lp.y = 2f + Variables.level * 3f;
		transform.position = lp;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
