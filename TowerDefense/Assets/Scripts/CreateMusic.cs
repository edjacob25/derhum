﻿using UnityEngine;
using System.Collections;

public class CreateMusic : MonoBehaviour {
	public GameObject musica;
	GameObject musicaExiste;
	Vector3 pos;
	// Use this for initialization
	void Start () {
		pos = this.transform.position;
		pos.y -= 2;
		musicaExiste = GameObject.Find("Music(Clone)");
		if (musicaExiste == null) {
			musicaExiste = Instantiate (musica, pos, Quaternion.identity)as GameObject;
		} else {
			musicaExiste.transform.position = pos;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
