﻿using UnityEngine;
using System.Collections;

public class TDmovement : MonoBehaviour {

	Vector3 pos;
	public float velocity = -0.05f;
	bool movimientoX;
	bool inicioJuego;
	public int hitPoints = 2;
	int vidaInicial;
	Animator anim;
	public GameObject deathParticle, crashParticle;
	public int ganancia;
	public GameObject vida;
	PlayerStats stats;
	bool	restado=false;
	Vector3 escalaOriginal;
	Vector3 rotation = new Vector3(0,90,0);
	GameObject hitCopy;

	// Use this for initialization
	void Start () {
		escalaOriginal = vida.transform.localScale;
		vidaInicial = hitPoints;
		pos = this.transform.position;
		movimientoX = true;
		inicioJuego = true;
		//anim = GetComponent<Animator> ();
		anim = GetComponentInChildren<Animator> ();
		stats = GameObject.Find ("ControlUI").GetComponent<PlayerStats> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (inicioJuego) {
			if (movimientoX) {
				pos.x = pos.x + velocity;
			} else {
				pos.z = pos.z + velocity;
			}
			this.transform.position = pos;
		}
		if (hitPoints < 1) 
			DieTorre ();
	}

	public void CambiarDireccion(string dir)
	{
		int y = 0;
		if(dir == "-x")
		{
			vida.transform.eulerAngles = rotation;
			movimientoX = true;
			velocity = - Mathf.Abs(velocity);
			y=0;
		}
		if(dir == "+x")
		{
			vida.transform.eulerAngles = rotation;
			movimientoX = true;
			velocity = Mathf.Abs(velocity);
			y=180;
		}
		if(dir == "-z")
		{
			vida.transform.eulerAngles = rotation;
			movimientoX = false;
			velocity = -Mathf.Abs(velocity);
			y=-90;
		}
		if(dir == "+z")
		{
			vida.transform.eulerAngles = rotation;
			movimientoX = false;
			velocity = Mathf.Abs(velocity);
			y= 90;
		}
		transform.eulerAngles = new Vector3 (0, y, 0);
	}

	public void IniciarJuego()
	{
		inicioJuego = true;
	}

	public void hit(int damage)
	{
		hitPoints -= damage;
		if(hitPoints>0)
			vida.transform.localScale = new Vector3 (escalaOriginal.x * hitPoints / vidaInicial, escalaOriginal.y, escalaOriginal.z);
		else
			vida.transform.localScale = new Vector3 (0, escalaOriginal.y, escalaOriginal.z);
	}

	public void Die()
	{
		hitCopy = Instantiate(crashParticle, this.transform.position, Quaternion.AngleAxis(90,Vector3.right)) as GameObject;
		GameObject.Find("SpawnZone").SendMessage("ReducirContador");
		Destroy (gameObject);
	}
	public void DieTorre()
	{
		hitCopy = Instantiate(deathParticle, this.transform.position, Quaternion.AngleAxis(135,Vector3.right)) as GameObject;
		velocity = 0;
		this.tag = "Untagged";
		transform.GetChild(0).tag = "Untagged";
		anim.Play ("death_new");
		Invoke ("destroyGO", 1);

	}

	public void destroyGO()
	{
		if (!restado) {
			GameObject.Find ("SpawnZone").SendMessage ("ReducirContador");
			restado=true;
		}
		stats.AumentarDinero(ganancia);
		Destroy (gameObject);
	}

}
