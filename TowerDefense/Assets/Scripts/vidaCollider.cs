﻿using UnityEngine;
using System.Collections;

public class vidaCollider : MonoBehaviour {

	GameObject UI;
	PlayerStats stats;
	public GameObject particles;
	ParticleSystem psTorre;
	// Use this for initialization
	void Start () {
		stats = GameObject.Find ("ControlUI").GetComponent<PlayerStats> ();
		psTorre = particles.GetComponent<ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag == "Enemy") {
			c.gameObject.SendMessage("Die",SendMessageOptions.DontRequireReceiver);
			stats.AumentarVida(-5);
		}
		if (c.gameObject.tag == "EnemyBoss") {
			c.gameObject.SendMessage("Die",SendMessageOptions.DontRequireReceiver);
			stats.AumentarVida(-20);
		}
	}

	public void ChangeColor(float proporcion)
	{
		float red = 252 - 252f * proporcion;
		float green = 200f * proporcion;
		float blue = 252f * proporcion;
		psTorre.startColor = new Color (red/252, green/252, blue/252, 0.5f);
	}
}
