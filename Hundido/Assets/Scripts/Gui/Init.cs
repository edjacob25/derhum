﻿using UnityEngine;
using System.Collections;

public class Init : MonoBehaviour {
	
	public GameObject loading;
	public GameObject Instructions;
	string host = "http://www.jbrr25.meximas.com/";
	bool answersDone = false;
	bool questionsDone = false;

	void Start () {
		Application.ExternalCall("getName","2");
		StartCoroutine("GetQuestions");
		StartCoroutine("GetAnswers");

		loading.transform.localScale = new Vector3(Screen.width,Screen.height);

	}
	
	void setName(string name)
	{
		Variables.name = name;
	}
	IEnumerator GetQuestions()
	{
		string questions;
		WWW www;
		string target = host + "getQuestions.php";
		www = new WWW (target);
		//Debug.Log("getting data");
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
		else
		{
			questions = www.text;
			Debug.Log(questions);
			Variables.questions = questions.Split(new char[]{'&'});
			questionsDone = true;
			RemoveScreen();
		}
	}
	
	IEnumerator GetAnswers()
	{ 
		string answers;
		WWW www;
		string target = host + "getAnswers.php";
		www = new WWW (target);
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
		else
		{
			answers = www.text;
			Debug.Log(answers);
			Variables.answers = answers.Split(new char[]{'&'});
			answersDone = true;
			RemoveScreen();
		}
	}


	void RemoveScreen()
	{
		if (answersDone && questionsDone)
		{
			loading.SetActive(false);
		}
	}

	void Update()
	{
		if (answersDone && questionsDone && Input.GetKey(KeyCode.Space))
		{
			Application.LoadLevel("game");
		}
	}
}
