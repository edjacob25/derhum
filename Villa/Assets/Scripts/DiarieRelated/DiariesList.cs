﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

public class DiariesList : MonoBehaviour {
	
	public TextAsset diaries;
    public GameObject prefab;
	string[,] text= new string[30,2];
	GameObject[] all = new GameObject[30];
	public GameObject[] message;

	private bool LoadText()
	{

		string line;
		int lines = 0;
		int total = 0;
		StringReader reader = new StringReader(diaries.text);
		using (reader)
		{
			do
			{
				line = reader.ReadLine();
				if (line != null)
				{
					if (line.Equals(""))
					{
						//Espacio vacío para inicializar
						lines = 0;
					}
					else
					{
						lines++;
						if (lines == 1)
						{
							//Titulo
							text[total, 0] = line;
							total++;
						}
						else
						{
							//Mensaje
							text[total - 1, 1] = line;
						}
					}
				}
			} while (line != null);
			reader.Close();
			return true;
		}
	}

	void Awake()
	{
		LoadText();
	}
	// Use this for initialization
	void Start () {
		for (int i = 0; i < 30; i++) {
			GameObject temp = NGUITools.AddChild(gameObject, prefab);
			all[i] = temp;
			temp.SendMessage("Init", i + 1);
			if (Variables.diaries[i] != true)
				temp.SetActive(false);
		}
		gameObject.GetComponent<UIGrid>().Reposition();
	}
	
	void Restart()
	{
		for (int i = 0; i < 30; i++) {
			if (Variables.diaries[i] == true)
				all[i].SetActive(true);
		}
		
	}

	void ListDiarie (int num) {
		num = num - 1;
		gameObject.SetActive(false);
		message[0].SetActive(true);
		message[0].GetComponentInChildren<UILabel>().text = text[num, 0];
		message[1].SetActive(true);
		message[1].GetComponentInChildren<UILabel>().text = text[num, 1];
		message[2].SetActive(true);
	}
}
