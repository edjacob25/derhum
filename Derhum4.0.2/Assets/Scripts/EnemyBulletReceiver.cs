﻿using UnityEngine;
using System.Collections;

public class EnemyBulletReceiver : MonoBehaviour {


	public GameObject mono;
	public GameObject canvasPregunta;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c){
		if (c.gameObject.tag == "Player") 
		{
			UnityEngine.Time.timeScale = 0.0f;
			canvasPregunta.SetActive(true);
			Destroy(mono);
		}
	}
}
