using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	
	public GameObject ball;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		float y = ball.transform.position.y;
		
		float error = Random.value - 0.5f;
		float diff = y- this.transform.position.y  + error;
		
		
		float x = this.transform.position.x;
		
		transform.position = new Vector3(x,this.transform.position.y + diff*0.05f,0);
		
		
		
		
	}
}
