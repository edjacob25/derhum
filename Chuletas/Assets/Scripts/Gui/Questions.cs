﻿using UnityEngine;
using System.Collections;

public class Questions : MonoBehaviour {
	public GameObject buttonA;
	public GameObject buttonB;
	public GameObject buttonC;
	public GameObject buttonD;
	public GameObject question;
	public GameObject instructions;
	public GameObject status;
	// Use this for initialization

	string[] questions = {
		"Usar los bienes de uso común como calles, semáforos, banquetas, parques y jardines es un derecho:",
		"Convivir de manera respetuosa con todas las personas es un derecho:",
		"La ley federal de los trabajadores al servicio del estado protege a:",
		"Fomentar la cultura democrática es un:",
		"Respetar la integridad física y emocional, la salud y la sexualidad de los demás es un derecho"
	};
	
	string[,] answers = {
		{"Social","De la mujer","Familiar","De los niños"},
		{"Social","De la mujer","Familiar","De los niños"},
		{"Trabajadores del estado",	"Mujeres", "Niños y jóvenes"," Jugadores de Futbol"	},
		{"Deber", "Derecho", "Juego", "Pensamiento"},
		{"Social","De la mujer","Familiar","De los niños"}
	}; //= 
	
	GameObject[] buttons = {null,null,null,null};
	
	void putQuestions(int numQuestion){
		int[] used = {-1,-1,-1,-1};
		bool diff = true;
		int pos = 0;
		for (int i = 0; i<4; i++)
		{
			do
			{
				pos = Mathf.FloorToInt(Random.value* 3.99f);
				if (used[pos] == -1)
				{
					diff = true;
					used[pos] = 1;
				}
				else
					diff = false;
			}while (diff == false);
			
			buttons[pos].GetComponentInChildren<UILabel>().text = answers[numQuestion,i];
			if (i == 0)
			{
				buttons[pos].tag = "RightGuess";
			}
		}
		question.GetComponent<UILabel>().text = questions[numQuestion];
		
	}

	void Start () {
		int i = Mathf.FloorToInt(Random.value*4.99f);
		//Debug.Log(i);
		buttons[0] = buttonA;
		buttons[1] = buttonB;
		buttons[2] = buttonC;
		buttons[3] = buttonD;
		putQuestions(i);
		/*buttonA.GetComponentInChildren<UILabel>().text =  answers[i,0];
		buttonA.tag = "RightGuess";
		buttonB.GetComponentInChildren<UILabel>().text =  answers[i,1];
		buttonC.GetComponentInChildren<UILabel>().text =  answers[i,2];
		buttonD.GetComponentInChildren<UILabel>().text =  answers[i,3];
		question.GetComponent<UILabel>().text = questions[i];*/

		string statusText, ins;
		UILabel sLabel = status.GetComponent<UILabel>();
		UILabel iLabel = instructions.GetComponent<UILabel>();
		if (Variables.level < 2)
		{
			if (Variables.winner == true)
			{
				statusText = "Ganador";
				sLabel.color = Color.green;
				ins = "Si deseas ir al siguiente nivel, responde adecuadamente la siguiente pregunta";
			}
			else
			{
				statusText = "Has perdido";
				sLabel.color = Color.red;
				ins = "Si deseas mantener el nivel, responde adecuadamente la siguiente pregunta";
			}
		}
		else
		{
			statusText = "Ganador";
			sLabel.color = Color.green;
			ins = "Tienes 5 derechitos, si deseas duplicar tus derechitos, responde adecuadamente la siguiente pregunta";
		}
		sLabel.text = statusText;
		iLabel.text = ins;
	}
	
	/*void putQuestions(int question){
		int[] used = {-1,-1,-1,-1};
		bool diff = true;
		float pos = 0;
		for (int i = 0; i<4; i++)
		{
			do
			{
				pos = Mathf.FloorToInt(Random.value* 3.99);
				if (used[pos] == -1)
				{
					diff == true;
					used[pos] == 1;
				}
			} while (diff == false)

			buttons[pos].GetComponentInChildren<UILabel>().text = answers[question,i];
			if (i == 0)
			{
				buttons[pos].name = "RightGuess";
			}
		}

	}*/

	// Update is called once per frame
	void Update () {
	
	}
}
