﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Contador : MonoBehaviour {

	public float derechitos;
	//public GUIText text;
	// Use this for initialization
	public Text text;
	public Text derechous;
	void Start () {
		derechitos = 0;
	
	}
	
	// Update is called once per frame
	void Update () {
		//text = gameObject.GetComponent<Text>;
		text.text = "GANASTE!\n Tienes "+derechitos+" Derechitos de recompensa";
		derechous.text = "Derechitos: "+derechitos;
	}

	void subirDerechitos() {
		derechitos += 50;
	}

	void subirDerechitos2() {
		derechitos += 10;
	}

	void bajarDerechitos() {
		derechitos -= 30;
		if (derechitos < 0) {
			derechitos = 0;
		}
	}

	float regresaDerechitos() {
		return derechitos;
	}
}
