﻿using UnityEngine;
using System.Collections;

public class MoveLaberynth : MonoBehaviour {

	private GameObject[] wall12;
	private GameObject[] wall13;
	private GameObject[] wall2;
	private GameObject[] wall3;
	private float change = 1;
	private float speedY = 0.09f;


	// Use this for initialization
	void Start () {

		wall12 = GameObject.FindGameObjectsWithTag("Wall 1.2");
		wall13 = GameObject.FindGameObjectsWithTag("Wall 1.3");
		wall2 = GameObject.FindGameObjectsWithTag("Wall 2");
		wall3 = GameObject.FindGameObjectsWithTag("Wall 3");



	}
	
	// Update is called once per frame
	void Update () {


		if (change == 1) 
		{
			//////////Pared cambia de 1 a 2
			float x3 = wall3[0].transform.position.x;  
			float y3 = wall3[0].transform.position.y;
			float z3 = wall3[0].transform.position.z;


			float x12 = wall12[0].transform.position.x;  
			float y12 = wall12[0].transform.position.y;
			float z12 = wall12[0].transform.position.z;
			
			float x13 = wall13[0].transform.position.x;  
			float y13 = wall13[0].transform.position.y;
			float z13 = wall13[0].transform.position.z;
			
			
			
			y3 -= speedY;

			y12 += speedY;
			y13 += speedY;
			
			
			if (y3 < -2.6) {y3 = -2.6f;}

			if (y12 > 2.5) {y12 = 2.5f;}
			if (y13 > 2.5) {y13 = 2.5f;}
			
			wall3[0].transform.position = new Vector3(x3,y3,z3);

			wall12[0].transform.position = new Vector3(x12,y12,z12);
			wall13[0].transform.position = new Vector3(x13,y13,z13);
		}


		if (change == 2) 
		{
			//////////Pared cambia de 1 a 2
			float x12 = wall12[0].transform.position.x;  
			float y12 = wall12[0].transform.position.y;
			float z12 = wall12[0].transform.position.z;

			float x2 = wall2[0].transform.position.x;  
			float y2 = wall2[0].transform.position.y;
			float z2 = wall2[0].transform.position.z;



			y12 -= speedY;

			y2 += speedY;

			if (y12 < -2.6) {y12 = -2.6f;}

			if (y2 > 2.5) {y2 = 2.5f;}

			wall12[0].transform.position = new Vector3(x12,y12,z12);

			wall2[0].transform.position = new Vector3(x2,y2,z2);
		}


		if (change == 3) 
		{
			//////////Pared cambia de 1 a 2
			float x2 = wall2[0].transform.position.x;  
			float y2 = wall2[0].transform.position.y;
			float z2 = wall2[0].transform.position.z;
			
			float x13 = wall13[0].transform.position.x;  
			float y13 = wall13[0].transform.position.y;
			float z13 = wall13[0].transform.position.z;
			
			
			
			float x3 = wall3[0].transform.position.x;  
			float y3 = wall3[0].transform.position.y;
			float z3 = wall3[0].transform.position.z;
			
			
			
			y2 -= speedY;
			y13 -= speedY;
			
			y3 += speedY;
			
			
			if (y2 < -2.6) {y2 = -2.6f;}
			if (y13 < -2.6) {y13 = -2.6f;}
			
			if (y3 > 2.5) {y3 = 2.5f;}
			
			wall2[0].transform.position = new Vector3(x2,y2,z2);
			wall13[0].transform.position = new Vector3(x13,y13,z13);
			
			wall3[0].transform.position = new Vector3(x3,y3,z3);
		}

	
}

	void Change(float r)
	{
		change = change + r;
		if (change > 3) {change = 1;}

	}
}
