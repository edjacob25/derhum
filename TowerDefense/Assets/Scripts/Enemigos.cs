﻿using UnityEngine;
using System.Collections;

public class Enemigos : MonoBehaviour {

	//public GameObject receiver;
	public float velocidad;
	public float movex,movez;
	float x,z,y;
	public float life;
	public GameObject spawner;
	//public bool doDestroy = true;

	
	// Use this for initialization
	void Start () {
	}
	
	void LifeLess(float l){
		life -= l;
		if (life < 0) {
			spawner.SendMessage("ReducirContador");
			Destroy (gameObject);

		}
	}
	void Attack()
	{
		Destroy (gameObject);
	}
	void ChangeMoveX(float move)
	{
		movex = move;
	}
	void ChangeMoveZ(float move)
	{
		movez = move;
	}
	
	void EnemyMove(){
		//Vector3 p = transform.position;
		x = transform.position.x;
		x += movex * (velocidad);
		z = transform.position.z;
		z += movez * (velocidad);
		y = transform.position.y;
		//p = new Vector3 (x, y, z);
		transform.position = new Vector3 (x, y, z);
	}


	// Update is called once per frame
	void Update () {
			EnemyMove();
	}

}
