﻿using UnityEngine;
using System.Collections;

public class RestartButtons : MonoBehaviour {
	void OnClick () {
		if (gameObject.name == "Main") {
			Application.ExternalCall("sendStatus","1");
			Application.ExternalCall("changeScene","0");
		}
		else {
			Variables.height = 0;
			Variables.level = 0;
			Variables.accumulatedScore = 0;
			Variables.bonus = 3;
			Application.LoadLevel("game");
		}
	}

	// Use this for initialization
	void Start () {
		Application.ExternalCall("sendStatus","1");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
