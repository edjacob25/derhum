﻿using UnityEngine;
using System.Collections;

public class luz : MonoBehaviour {

	public GameObject fire;
	GameObject turnosLabel;
	int turnos = 5;
	bool instruct = true;
	GameObject correctasLabel;

	GameObject buenaAcierto1;
	GameObject buenaAcierto2;
	GameObject buenaAcierto3;
	GameObject buenaAcierto4;
	GameObject buenaAcierto5;

	GameObject malaAcierto1;
	GameObject malaAcierto2;
	GameObject malaAcierto3;
	GameObject malaAcierto4;
	GameObject malaAcierto5;

	GameObject correcto;
	GameObject incorrecto;

	int correctas = 0;

	bool sinBoton = false;

	float pos1 = 2.61f;
	float pos2 = 1.78f;
	float pos3 = .81f;
	float pos4 = 0f;
	float pos5 = -.91f;


	float posZ5 = 2.37f;
	float posZ4 = 1.88f;
	float posZ3 = 1.38f;
	float posZ2 = .92f;
	float posZ1 = .5f;



	float posXfin5 = -.87f;
	float posXfin4 = 0f;
	float posXfin3 = .9f;
	float posXfin2 = 1.7f;
	float posXfin1 = 2.5f;

	float posZFin = .0f;

	GameObject bola;
	GameObject labelVictoria;
	GameObject pregunta;
	GameObject presionaA;
	//public GameObject backgroundA;
	GameObject etiqueta;
	GameObject preguntas;


	GameObject minion1;
	GameObject minion2;
	GameObject minion3;
	GameObject minion4;
	GameObject minion5;




  public int puertaCorrecta;

	bool inicio = false;
	bool posFinLuz = false;
	public bool saberSiYaPuedeApretarPuerta = false;
	int endPos;

	bool luzHaLlegadoAsuDestinoFinal = false;
	bool movimientoYNegativo = false;

	bool yaPuedesOlvidarEsteObjeto = false;

	bool yaRecibioRespuesta = false;

	// Use this for initialization
	void Start () {

		turnosLabel = GameObject.Find("turnos");
		correctasLabel  = GameObject.Find("puntuación");

	
		puertaCorrecta = Random.Range (1, 7);
		bola = GameObject.Find("Aldeano5");
		labelVictoria = GameObject.Find("labelVictoria");
		etiqueta = GameObject.Find("pregunta");
		pregunta = GameObject.Find("preguntaObjeto");
		//bola.SendMessage ("recibirFinal",puertaCorrecta);
		presionaA = GameObject.Find("presionaA");





		buenaAcierto1 = GameObject.Find("IntentoHermana1");
		buenaAcierto2 = GameObject.Find("IntentoHermana2");
		buenaAcierto3 = GameObject.Find("IntentoHermana3");
		buenaAcierto4 = GameObject.Find("IntentoHermana4");
		buenaAcierto5 = GameObject.Find("IntentoHermana5");

		malaAcierto1 = GameObject.Find("IntentoMala1");
		malaAcierto2 = GameObject.Find("IntentoMala2");
		malaAcierto3 = GameObject.Find("IntentoMala3");
		malaAcierto4 = GameObject.Find("IntentoMala4");
		malaAcierto5 = GameObject.Find("IntentoMala5");

		minion1 = GameObject.Find("Aldeano1");
		minion2 = GameObject.Find("Aldeano2");
		minion3 = GameObject.Find("Aldeano3");
		minion4 = GameObject.Find("Aldeano5");


		correcto = GameObject.Find("Correcto");
		incorrecto = GameObject.Find("Incorrecto");

		buenaAcierto1.SetActive (false);
		buenaAcierto2.SetActive (false);
		buenaAcierto3.SetActive (false);
		buenaAcierto4.SetActive (false);
		buenaAcierto5.SetActive (false);

		malaAcierto1.SetActive (false);
		malaAcierto2.SetActive (false);
		malaAcierto3.SetActive (false);
		malaAcierto4.SetActive (false);
		malaAcierto5.SetActive (false);

		correcto.SetActive (false);
		incorrecto.SetActive (false);

		Vector3 lp = transform.position;

		if (puertaCorrecta == 1)
		{
			lp.z = posZ1;
			lp.x = pos1;
		}

		if (puertaCorrecta == 2)
		{
			lp.z = posZ2;
			lp.x = pos2;
		}

		if (puertaCorrecta == 3)
		{
			lp.z = posZ3;
			lp.x = pos3;
		}

		if (puertaCorrecta == 4)
		{
			lp.z = posZ4;
			lp.x = pos4;
		}

		if (puertaCorrecta == 5)
		{
			lp.z = posZ5;
			lp.x = pos5;
		}

		transform.position = lp;
	}

	void cambiarLabel()
	{
		sinBoton = true;
	}
	// Update is called once per frame
	void Update () {
				Vector3 lp = transform.position;

		turnosLabel.GetComponentInChildren<UILabel> ().text = "Turnos Restantes : " + turnos;
		correctasLabel.GetComponentInChildren<UILabel> ().text = "Puntos: " + correctas*100;
		if(instruct == true && Input.GetKey(KeyCode.Space))
		{
			GameObject.Find("Instructions").SetActive(false);
			instruct = false;
		}


		if (turnos == 0 || correctas == 3)
		{
			//turnosLabel.GetComponentInChildren<UILabel> ().text = "";
			//correctasLabel.GetComponentInChildren<UILabel> ().text = "";
			//yaPuedesOlvidarEsteObjeto = true;


			if(correctas == 3)
			{
				Variables.winner = true;
				Application.LoadLevel("Pregunta");
				//presionaA.GetComponentInChildren<UILabel> ().text = "Responde Correctamente para duplicar tus derechitos";
			}
			else{
				Application.LoadLevel("restart");
			}

		}

		if (turnos <= 3)
		{
			bola.SendMessage("recibirNumeroDeTurnos");
		}

		if(yaPuedesOlvidarEsteObjeto == true)
		{
			if (Input.GetKeyDown ("space")) {

				Debug.Log ("DEBERIA PONERSE EL FONDO");
				presionaA.GetComponentInChildren<UILabel> ().text = "responde a la pregunta";
				etiqueta.GetComponentInChildren<UILabel> ().text = "POR QUE";
				pregunta.SendMessage("initQuestion");

			}
		}
				if (Input.GetKeyDown ("space")) {
						inicio = true;
			
				}


		/*
		if (movimientoYNegativo == true) {
			lp.y = transform.position.y - .05f;
			transform.position = lp;

				}
				if (inicio == true) {
						lp.y = transform.position.y + .05f;
						transform.position = lp;
				}

				if (lp.y > 5) {
						inicio = false;
						posFinLuz = true;
				}
		if (lp.y < 2.3) {
			movimientoYNegativo = false;
				}

				if (posFinLuz == true) {
						if (endPos == 1) {
								lp.x = posXfin1;
						}

						if (endPos == 2) {
								lp.x = posXfin2;
						}

						if (endPos == 3) {
								lp.x = posXfin3;
						}

						if (endPos == 4) {
								lp.x = posXfin4;
						}

						if (endPos == 5) {
								lp.x = posXfin5;
						}

						
			            lp.z = posZFin;
						transform.position = lp;
						
			posFinLuz = false;
	
				}
				*/
		}
	void ultimaPos(int posFin)
	{
		endPos = posFin;
	}

	void yaPuedePuerta()
	{
		saberSiYaPuedeApretarPuerta = true;
	}

	void reinicio()
	{

		/*minion1.SendMessage("parado");
		minion2.SendMessage("parado");
		minion3.SendMessage("parado");
		minion4.SendMessage("parado");*/

		 inicio = false;
		 posFinLuz = false;
		 saberSiYaPuedeApretarPuerta = false;
		 luzHaLlegadoAsuDestinoFinal = false;
		 movimientoYNegativo = false;
		 yaPuedesOlvidarEsteObjeto = false;
		 yaRecibioRespuesta = false;
		 //endPos = 0;

		puertaCorrecta = Random.Range (1, 7);
		bola = GameObject.Find("Aldeano5");
		labelVictoria = GameObject.Find("labelVictoria");
		//bola.SendMessage ("recibirFinal",puertaCorrecta);
		presionaA = GameObject.Find("presionaA");
		Vector3 lp = transform.position;
		
		if (puertaCorrecta == 1)
		{
			lp.z = posZ1;
			lp.x = pos1;
		}
		
		if (puertaCorrecta == 2)
		{
			lp.z = posZ2;
			lp.x = pos2;
		}
		
		if (puertaCorrecta == 3)
		{
			lp.z = posZ3;
			lp.x = pos3;
		}
		
		if (puertaCorrecta == 4)
		{
			lp.z = posZ4;
			lp.x = pos4;
		}
		
		if (puertaCorrecta == 5)
		{
			lp.z = posZ5;
			lp.x = pos5;
		}
		
		if (puertaCorrecta == 6)
		{
			
		}
		
		transform.position = lp;

		}

	void olvidar()
	{
		correcto.SetActive (false);
		incorrecto.SetActive (false);
	}
	void saberPuerta(int numero)
	{

		if (saberSiYaPuedeApretarPuerta == true) {

			if (yaRecibioRespuesta == false) {
				if (numero == endPos) {
					Debug.Log ("LA ENDPOS = "+ endPos);	
					Debug.Log("EL NUMERO = "+numero);
					Debug.Log("DEBERIAS GANAR");

					presionaA.GetComponentInChildren<UILabel> ().text = "Presiona Barra de Espacio para continuar";
					//etiqueta.GetComponentInChildren<UILabel> ().text = "¡VICTORIA!";
					Instantiate(fire);

				    saberSiYaPuedeApretarPuerta = false;
					bola.SendMessage ("apretoLaPuerta");
					movimientoYNegativo = true;
					//yaPuedesOlvidarEsteObjeto = true;
					correctas ++;
					Variables.money = correctas*100;
					bola.SendMessage ("reiniciarVariables");

					reinicio();




					if (turnos == 5)
					{
						buenaAcierto1.SetActive (true);
					}
					if (turnos == 4)
					{
						buenaAcierto2.SetActive (true);
					}
					if (turnos == 3)
					{
						buenaAcierto3.SetActive (true);
					}
					if (turnos == 2)
					{
						buenaAcierto4.SetActive (true);
					}
					if (turnos == 1)
					{
						buenaAcierto5.SetActive (true);
					}

					minion1.SendMessage("parado");
					minion2.SendMessage("parado");
					minion3.SendMessage("parado");
					//minion4.SendMessage("parado");


					correcto.SetActive (true);
					Invoke("olvidar",3);
					turnos --;
				} 

				else 
				{

					minion1.SendMessage("burla");
					minion2.SendMessage("burla");
					minion3.SendMessage("burla");
					//minion4.SendMessage("burla");



					Debug.Log ("LA ENDPOS = "+ endPos);	
					Debug.Log("EL NUMERO = "+numero);
					Debug.Log("DEBERIAS PERDER");

					saberSiYaPuedeApretarPuerta = false;
					presionaA.GetComponentInChildren<UILabel> ().text = "Presiona Barra de Espacio Para Continuar";
					//etiqueta.GetComponentInChildren<UILabel> ().text = "¡Derrotado!";
				//	

					bola.SendMessage ("apretoLaPuerta");
					movimientoYNegativo = true;
					//yaPuedesOlvidarEsteObjeto = true;
					Debug.Log(bola);
					bola.SendMessage ("reiniciarVariables");


					if (turnos == 5)
					{
						malaAcierto1.SetActive (true);
					}
					if (turnos == 4)
					{
						malaAcierto2.SetActive (true);
					}
					if (turnos == 3)
					{
						malaAcierto3.SetActive (true);
					}
					if (turnos == 2)
					{
						malaAcierto4.SetActive (true);
					}
					if (turnos == 1)
					{
						malaAcierto5.SetActive (true);
					}
					incorrecto.SetActive (true);
					Invoke("olvidar",3);
					reinicio();
					turnos --;
								
					}

				}
		}

}
}
