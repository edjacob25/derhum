using UnityEngine;
using System.Collections;

public class DynamicEnemy : MonoBehaviour {
	
	GameObject player;
	CharacterController controller;
	public GameObject receiver;
	bool follow = false;

	
	// Use this for initialization
	void Start () {
		player  = GameObject.FindGameObjectWithTag("Player");
		controller = this.GetComponent<CharacterController>();
		
	}
	

	
	void EnemyMove(){
		Vector3 p = player.transform.position;
		p.y = this.transform.position.y;
		this.transform.LookAt(p);	
		controller.Move( transform.TransformDirection(Vector3.forward)*.07f );
	} 
	

	
	// Update is called once per frame
	void Update () {
		
		
		if( follow ){
			EnemyMove();
		}
	}
	
	void StartFollow(){
		
		follow = true;
		
	}
	
	void StopFollow(){
		
		follow = false;
		
	}
}
