using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	//public GameObject destine;
	public GameObject ans;
	public GameObject[] tables;
	public GameObject result;
	public GameObject particle;
	public GameObject tableParticle;

	void OnClick()
	{
		//GameObject.Find("Balls").SetActive(true);

		if (gameObject.tag == "RightGuess")
		{ 
			Instantiate(particle);
			if (Variables.points >= 10)
			{
				Variables.winner = true;
				StartCoroutine(ShowResult());
			}
			else
			{
				Variables.points ++;
			}
			//GameObject.Find("Mage").GetComponent<Mage>().getDamage();
		}
		else
		{
			Instantiate(tableParticle, tables[Variables.errors].transform.position, Quaternion.identity);
			tables[Variables.errors].SetActive(false);
			Variables.errors++;
			if(Variables.errors > 3)
			{
				StartCoroutine(ShowResult());
			}
			//GameObject.Find("Player").GetComponent<Player>().getDamage();
		}
		StartCoroutine(ShowAnswers());


	}
	// Use this for initialization

	IEnumerator ShowResult()
	{
		if (Variables.winner == true)
			result.GetComponent<UILabel>().text = "Ganaste!";
		else
			result.GetComponent<UILabel>().text = "Ahogado";
		yield return new WaitForSeconds(2);
		Application.LoadLevel("restart");
	}

	IEnumerator ShowAnswers()
	{
		ans.SetActive(true);

		GameObject.Find("Game").GetComponent<FillGame>().OrderAnswers();

		yield return new WaitForSeconds(2);
		ans.SetActive(false);
		//destine.SetActive(true);
		//GameObject.Find("Camera").GetComponent<Intro>().Initialize();
		GameObject.Find("Game").GetComponent<FillGame>().Initialize();
	}

	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
