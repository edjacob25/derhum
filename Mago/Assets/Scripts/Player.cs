﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public GameObject full;
	public GameObject second;
	public GameObject third;
	public GameObject forth;

	int[] life = {1,2,3,4};
	int templife;


	void DestroyParticle()
	{

		GameObject[] arr = GameObject.FindGameObjectsWithTag("PlayerLife");
		Destroy(arr[arr.Length-1]);
	}

	// Use this for initialization
	void Start () {
		GameObject main = null;
		templife = life[Variables.Magelife-1];
		if(templife == 4)
		{
			main = full;
		}
		if(templife == 3)
		{
			main = second;
		}
		if(templife == 2)
		{
			main = third;
		}
		
		if(templife == 1)
		{
			main = forth;
		}
		for (int i = 0; i < Variables.Playerlife; i++)
		{
			Instantiate(main, new Vector3(9,6-i*2,5), Quaternion.Euler(270,0,0));
		}
	}

	public void getDamage(){
		templife--;
		GameObject main = null;
		DestroyParticle();
		if(templife == 3)
		{
			main = second;
		}
		if(templife == 2)
		{
			main = third;
		}
		
		if(templife == 1)
		{
			main = forth;
		}

		Instantiate(main, new Vector3(9,6 - ((Variables.Playerlife-1)* 2),5), Quaternion.Euler(270,0,0));
	}

	// Update is called once per frame
	void Update () {
		if(templife < 1)
		{
			Variables.Playerlife --;
			Application.LoadLevel("game");
		}
		if(Variables.Playerlife < 1)
		{
			//Application.LoadLevel("Perdiste");

			Application.ExternalCall("changeScene","game");
			Debug.Log("Me regreso");
		}
	}
}
