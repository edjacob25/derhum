﻿using UnityEngine;
using System.Collections;

public class LabelStatus : MonoBehaviour {
	public GameObject points;
	string host ="http://www.jbrr25.meximas.com/";
	int val = 0;
	// Use this for initialization
	void Start () {

		if (Variables.winner){
			GetComponent<UILabel>().color = Color.green;
			GetComponent<UILabel>().text = "¡Ganador!";
			val = 2;
		}
		else {
			GetComponent<UILabel>().color = Color.red;
			GetComponent<UILabel>().text = "¡Has perdido!";
			val = 1;
		}
		points.GetComponent<UILabel>().text = "Felicidades" +Variables.name +" conseguiste "+val*20*Variables.points+" Derechitos, muy bien";
		StartCoroutine(SetMoney());
	}

	IEnumerator SetMoney()
	{
		
		WWW www;
		WWWForm form;
		form = new WWWForm();
		string target = host + "addMoney.php";
		form.AddField("username",Variables.name);
		form.AddField("money",Variables.points*20*val);
		www = new WWW (target,form);
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
		else
			Debug.Log(www.text);
	}
	// Update is called once per frame
	void Update () {
	
	}
}
