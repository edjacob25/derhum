﻿using UnityEngine;
using System.Collections;

public class VillagersMovement : MonoBehaviour {

	//int counter = 1;

	int rotation = 5;
	bool dandoDandoVuelta = false;
	//bool puedeCaminar = true;
    
    float time = 0;
	// Use this for initialization
	void Start () {
        time = Time.time + 5;
	}

    void RotateMe()
    {

        if (rotation >= 180)
        {
            dandoDandoVuelta = false;
            time = Time.time + 5;
        }
        else
        {
            transform.Rotate(0, 5, 0);
            rotation += 5;
            Invoke("RotateMe", 0.01f);
        }
    }

	// Update is called once per frame
	void Update () {
            
    	if (dandoDandoVuelta == false) {
    		if (Time.time > time) {
		    	dandoDandoVuelta = true;
                rotation = 0;
                Invoke("RotateMe", 0.01f);
    		}
				
	    } 
        /*else {
    		/*transform.Rotate (0, 5, 0);
            rotation = rotation - 5;
		    if (rotation <= 0) {
               
		        dandoDandoVuelta = false;
                time = Time.time + 5;
    		}
        }*/	
	}
}
