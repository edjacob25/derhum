﻿using UnityEngine;
using System.Collections;

public class FillGame : MonoBehaviour {
	public GameObject buttonA;
	public GameObject buttonB;
	public GameObject buttonC;
	public GameObject buttonD;
	public GameObject question;
	// Use this for initialization
	
	GameObject[] buttons = {null,null,null,null};

	void putQuestions(int numQuestion){
		int[] used = {-1,-1,-1,-1};
		bool diff = true;
		int pos = 0;
		string[] ans = Variables.answers[numQuestion].Split(new char[]{'#'});


		for (int i = 0; i<4; i++)
		{
			do
			{
				pos = Mathf.FloorToInt(Random.value* 3.99f);
				if (used[pos] == -1)
				{
					diff = true;
					used[pos] = 1;
				}
				else
					diff = false;
			}while (diff == false);
			
			buttons[pos].GetComponentInChildren<UILabel>().text = ans[i];
			if (i == 0)
			{
				buttons[pos].tag = "RightGuess";
			}
		}
		question.GetComponent<UILabel>().text ="Si deseas duplicar tus derechitos, responde correctamente\n\n"+ Variables.questions[numQuestion];
		
	}

	void Start () {
		//Debug.Log(i);
		buttons[0] = buttonA;
		buttons[1] = buttonB;
		buttons[2] = buttonC;
		buttons[3] = buttonD;
		int qs = Mathf.FloorToInt(Random.value*(Variables.questions.Length-1.01f));
		putQuestions(qs);
		/*buttonA.GetComponentInChildren<UILabel>().text =  answers[i,0];
		buttonA.tag = "RightGuess";
		buttonB.GetComponentInChildren<UILabel>().text =  answers[i,1];
		buttonC.GetComponentInChildren<UILabel>().text =  answers[i,2];
		buttonD.GetComponentInChildren<UILabel>().text =  answers[i,3];*/
	}

	public void Initialize()
	{
		for(int i = 0; i<4; i++)
		{
			buttons[i].tag = "Untagged";
		}
		int qs = Mathf.FloorToInt(Random.value*(Variables.questions.Length-1.01f));
		putQuestions(qs);
	}
	
	public void OrderAnswers()
	{
		GameObject[] arr = GameObject.FindGameObjectsWithTag("Answers");
		int pos = 0;
		for(int i = 0; i < 4; i++)
		{

			Vector3 lp = buttons[i].transform.localPosition;
			lp.y = lp.y -50;
			if (buttons[i].tag =="RightGuess"){
				pos = i;
			}
			arr[i].transform.localPosition = lp;

		}
		if(pos != 3)
		{
			Vector3 temp = arr[pos].transform.localPosition;
			arr[pos].transform.localPosition = arr[3].transform.localPosition;
			arr[3].transform.localPosition = temp;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
