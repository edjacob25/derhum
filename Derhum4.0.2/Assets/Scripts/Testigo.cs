﻿using UnityEngine;
using System.Collections;

public class Testigo : MonoBehaviour {

	public GameObject player;
	public GameObject canvasHeart;
	public GameObject canvasDerechitos;
	public GameObject contador;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c){
		
		if (c.gameObject.tag == "Player")
		{
			canvasHeart.SendMessage("Found", 1, SendMessageOptions.DontRequireReceiver);
			canvasDerechitos.SetActive(true);
			player.SendMessage ("Found", 1, SendMessageOptions.DontRequireReceiver);
			contador.SendMessage("subirDerechitos",1,SendMessageOptions.DontRequireReceiver);
			Destroy (gameObject);
		}

	}
}
