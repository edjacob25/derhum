﻿using UnityEngine;

using System.Collections;

using System.Collections.Generic;

using Edelweiss.DecalSystem;



public class RuntimeDecalsUpdater : MonoBehaviour {
	
	
	
	private DS_Decals m_Decals;
	
	private List <DS_DecalProjector> m_UsedDecalProjectors = new List <DS_DecalProjector> ();
	
	private List <WrappedDecalProjector> m_UsedWrappedDecalProjectors = new List <WrappedDecalProjector> ();
	
	
	
	private MeshFilter[] m_AffectedMeshFilters;
	
	
	
	private DecalsMesh m_DecalsMesh;
	
	private DecalsMeshCutter m_DecalsMeshCutter;
	
	GameObject[] all;
	
	private void Awake () {
		
		
		
		// Dummy to avoid warnings in console.
		
		//if (m_AffectedMeshFilters == null) {



		all = GameObject.FindGameObjectsWithTag ("MeshFilter");
		//Debug.Log (all.Length);

		m_AffectedMeshFilters = new MeshFilter[all.Length];

		for (int i = 0; i < all.Length; i++) {
			m_AffectedMeshFilters[i] = all[i].GetComponent<MeshFilter>();
		}
		//m_AffectedMeshFilters = new MeshFilter [0];
			
		//}
		
		
		
		m_Decals = GetComponent <DS_Decals> ();
		
		m_DecalsMesh = new DecalsMesh (m_Decals);
		
		m_DecalsMeshCutter = new DecalsMeshCutter ();
		
		
		
		DS_DecalProjector[] l_DecalProjectors = GetComponentsInChildren <DS_DecalProjector> ();
		
		foreach (DS_DecalProjector l_Projector in l_DecalProjectors) {
			
			UpdateProjector (l_Projector);
			
		}
		
	}
	
	
	
	public void RemoveProjector (DS_DecalProjector a_Projector) {
		
		RemoveProjectorInternal (a_Projector);
		
		m_Decals.UpdateDecalsMeshes (m_DecalsMesh);
		
	}
	
	
	
	private void RemoveProjectorInternal (DS_DecalProjector a_Projector) {
		
		if (m_UsedDecalProjectors.Contains (a_Projector)) {
			
			int l_Index = m_UsedDecalProjectors.IndexOf (a_Projector);
			
			WrappedDecalProjector l_WrappedDecalProjector = m_UsedWrappedDecalProjectors [l_Index];
			
			m_DecalsMesh.RemoveProjector (l_WrappedDecalProjector);
			
			m_UsedDecalProjectors.RemoveAt (l_Index);
			
			m_UsedWrappedDecalProjectors.RemoveAt (l_Index);
			
		}
		
	}
	
	
	
	public void UpdateProjector (DS_DecalProjector a_Projector) {
		
		RemoveProjectorInternal (a_Projector);
		
		
		
		WrappedDecalProjector l_WrappedDecalProjector = new WrappedDecalProjector (a_Projector);
		
		m_UsedDecalProjectors.Add (a_Projector);
		
		m_UsedWrappedDecalProjectors.Add (l_WrappedDecalProjector);
		
		
		
		m_DecalsMesh.AddProjector (l_WrappedDecalProjector);
		
		
		
		foreach (MeshFilter l_MeshFilter in m_AffectedMeshFilters) {

			Mesh l_Mesh = l_MeshFilter.sharedMesh;
			
			Transform l_Transform = l_MeshFilter.transform;
			
			Matrix4x4 l_WorldToMesh = l_Transform.worldToLocalMatrix;
			
			Matrix4x4 l_MeshToWorld = l_Transform.localToWorldMatrix;
			
			m_DecalsMesh.Add (l_Mesh, l_WorldToMesh, l_MeshToWorld);
			
		}
		
		
		
		m_DecalsMeshCutter.CutDecalsPlanes (m_DecalsMesh);
		
		m_DecalsMesh.OffsetActiveProjectorVertices ();
		
		m_Decals.UpdateDecalsMeshes (m_DecalsMesh);
		
	}
	
}