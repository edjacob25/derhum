using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	public GameObject destine;
	public GameObject ans;
	public GameObject particle;

	void OnClick()
	{
		//GameObject.Find("Balls").SetActive(true);

		if (gameObject.tag == "RightGuess")
		{
			GameObject.Find("Mage").GetComponent<Mage>().getDamage();
			Instantiate(particle,new Vector3(0,1,-3) , Quaternion.identity); 
		}
		else
		{
			GameObject.Find("Player").GetComponent<Player>().getDamage();
		}
		StartCoroutine(ShowAnswers());


	}
	// Use this for initialization

	public IEnumerator ShowAnswers()
	{
		ans.SetActive(true);

		GameObject.Find("Game").GetComponent<FillGame>().OrderAnswers();

		yield return new WaitForSeconds(2);
		ans.SetActive(false);
		destine.SetActive(true);
		GameObject.Find("Camera").GetComponent<Intro>().Initialize();
		GameObject.Find("Game").SetActive(false);
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
