using UnityEngine;
using System.Collections;

public class Variables {

	public static int level = 0;
	public static int height = 0;
	public static bool winner;
	public static bool music = false;
	public static int accumulatedScore = 0;
	public static int bonus = 3;
	public static string[] questions;
	public static string[] answers;
	public static string name;
}
