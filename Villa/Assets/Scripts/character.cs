﻿using UnityEngine;
using System.Collections;
using Edelweiss.DecalSystem;


public class character : MonoBehaviour {

	GameObject cuadroDeDialogo;
	GameObject dialogo;
	GameObject informacion;
	GameObject nuevoItem;
	GameObject personajeDice;
	GameObject derechitosLabel;
	GameObject diariosLabel;
	public GameObject instru;
	public GameObject particlebuy;
	public GameObject back;

	GameObject puerta1;
	GameObject puerta2;

	bool[] controladorVictorias = {false,false,false};

	GameObject niñaJuguetonaObjeto;
	GameObject aldeanoPensativoObjeto;
	GameObject mujerPreocupada;
	GameObject madreLlorona;
	GameObject niñoPerdidoObject;
	bool cercaNiñoEncerrado = false;
	//bool niñoTeDioDerechitos = false;
	string dialogoString = "";
	string informacionString = "";
	string nuevoItemString = "";
	string personajeDiceString = "";
	string derechitosString;
	string diariosString;

	public int derechitosNumeros = 0;

	bool tieneLLavedDeLaTorre = false;

	bool cercaDePuerta = false;

	bool haAbiertoPuertaTorre = false;
	bool cercaDePuertaTorre = false;
	GameObject puertaTorre;

	bool inicio = true;
	bool cercaDeAldeana = false;
	bool cercaDeMujerPreocupada = false;
	bool cercaDeMercader1 = false;
	bool cercaDeMercader2 = false;
	bool cercaDeMercader3 = false;
	bool cercaDeMercader4 = false;

	bool cercaDeNiñaJuguetona = false;
	bool cercaDeAldeanoPensativo = false;
	bool cercaDeAldeanoCansado = false;
	bool cercaDeAldeanoCansado2 = false;

	bool cercaRetoChuletas = false;
	bool cercaNiñoPerdido = false;

	bool cambioPuertaDecir = false;
	bool yaAbrioLaPuerta = false;

	bool inicioDialogoConMarcela = false;
	bool segundoDialogoDeMarcela = false;
	bool finalizarDialogoMarcela = false;
	//bool totalEndConversation = false;
	bool tieneLlave = false;

	bool cercaHundido = false;
	//bool cercaNiñoEencerrado = false;
	bool haSalvadoAlNiñoEncerrado = false;

	GameObject planta1;
	GameObject planta2;
	GameObject buro;
	GameObject cama;
	GameObject escritorio;
	GameObject librero;

	//public DS_Decals decalsController;
	public RuntimeDecalsUpdater updater;

	bool cercaDePlanta1 = false;
	bool cercaDePlanta2 = false;
	bool cercaDeBuro = false;
	bool cercaDeCama = false;
	bool cercaDeEscritorio = false;
	bool cercaDeLibrero = false;
	bool cercaNiñaCuriosa = false;

	bool cercaDeMago = false;
	GameObject mago;

	//GameObject telaraasAhorcado;
	//GameObject telaraasPeekAboo;
	//GameObject telaraasChuleta;
    	
	int fragmentosDiario = 0;

    bool[] diarios = new bool[30];

	int segundaConv = 0;

	//bool postConversacion1 = false;
	//bool postConversacionEnd = false;

	private DecalsMesh dm;
    public GameObject hangWebs;
    public GameObject chuletasWebs;
    public GameObject otherWebs;

    public GameObject panel2_1;
    public GameObject panel2_2;
    public GameObject panel4;

	IEnumerator getMoney()
	{
		WWW www;
		WWWForm form;
		string target = "http://lad.tol.itesm.mx/derhum/getProperty.php";
		form = new WWWForm();
		form.AddField("property","money");
		form.AddField("username",Variables.name);
		www = new WWW (target,form);
		Debug.Log("getting Money");
		//while(!www.isDone)
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
		else 
		{
			Debug.Log(www.text);
			derechitosNumeros = int.Parse(www.text);
		}

		yield return new WaitForSeconds(30);
		StartCoroutine("getMoney");
	}

    void AddDiarie()
    {
        fragmentosDiario++;
    }

	// Use this for initialization
	void Start () {

		StartCoroutine("getMoney");
		//dm = new DecalsMesh (decalsController);
		Application.ExternalCall("setName",Variables.name);

		cuadroDeDialogo=GameObject.Find("cuadrodeDialogo");
		dialogo=GameObject.Find("dialogo");
		informacion=GameObject.Find("informacion");
		nuevoItem=GameObject.Find("nuevoItem");
		personajeDice=GameObject.Find("PersonajeDice");
		diariosLabel =GameObject.Find("diariosLabel");
		derechitosLabel = GameObject.Find("derechitosLabel");
		puerta1 = GameObject.Find ("puertaDoble1");
		puerta2 = GameObject.Find ("puerta2");
		puertaTorre = GameObject.Find ("towerDoor");
		niñoPerdidoObject = GameObject.Find ("niñoPerdido");

		niñaJuguetonaObjeto = GameObject.Find ("niñaJuguetona");
		aldeanoPensativoObjeto = GameObject.Find ("aldeanoPensativo");
		mujerPreocupada = GameObject.Find ("mujerPreocupada");

		//telaraasChuleta = GameObject.Find ("RedesChuletas");
		//telaraasAhorcado  = GameObject.Find ("RedesAhorcado");
		//telaraasPeekAboo = GameObject.Find ("RedesPeekaBoo");

		mago = GameObject.Find ("mago");
		mago.gameObject.SetActive (false);

	    cuadroDeDialogo.gameObject.SetActive (false);

		librero = GameObject.Find ("libroCrate");
		escritorio = GameObject.Find ("crateDesk");
		planta1 = GameObject.Find ("planta");
		planta2 = GameObject.Find ("planta2");
		cama = GameObject.Find ("cama");
		buro = GameObject.Find ("buro");
        
	}
	
	void desaparecer()
	{
		nuevoItemString = "";
		//dialogoString = "";

    }

	void GanasteMinijuego(int juego)
	{
		string windowType = "";
		SendMessage("StopMusic");
		GameObject dest = null;
		//Juego 1 = chuletas
		//Juego 2 = ahogado
		//Juego 3 = peeABOO
		controladorVictorias [juego-1] = true;
		//LimpiarVilla
		int r = 0;
		for (int t = 0; t<3; t++)
		{
			//Debug.Log(controladorVictorias[t]);
			if(controladorVictorias[t] == true)
			{
				r ++;
			}
		}
		if(r == 1) {
			dest = chuletasWebs;
			windowType = "ChuletasElemnts";
		}
		if(r == 2) {
			dest = hangWebs;
			windowType = "AhorcadoElemnts";
		}
		if(r == 3) {
			dest = otherWebs;
			windowType = "PeekaBooElemnts";
			Debug.Log ("Mago activado");
			mago.gameObject.SetActive (true);
		}

		Debug.Log(transform.GetChild(1).name);
		transform.GetChild(1).SendMessage("ChangeSkyboxColor", r);

		for( int i = 0 ; i < dest.transform.childCount ; i++ ){
			updater.RemoveProjector( dest.transform.GetChild(i).GetComponent<DS_DecalProjector>() );
			
		}
	    
		GameObject[] windows = GameObject.FindGameObjectsWithTag(windowType);

		for(int i = 0; i <windows.Length -1; i++){
			//Debug.Log(windows[i].name);
			updater.RemoveProjector(windows[i].GetComponent<DS_DecalProjector>()); 
		}


	}

	IEnumerator setMoney()
	{
		WWW www;
		WWWForm form;
		string target = "http://lad.tol.itesm.mx/derhum/setProperty.php";
		form = new WWWForm();
		form.AddField("property","money");
		form.AddField("username",Variables.name);
		form.AddField("value",derechitosNumeros);
		www = new WWW (target,form);
		
		//while(!www.isDone)
		yield return www;
		
		if (www.error != null) 
			Debug.LogError (www.error);
	}
    
	void BackMinigame()
	{
		StartCoroutine("getMoney");
	}

	void Update () {

		/*if (Input.GetKeyDown ("c"))
		{

			GanasteMinijuego(a);
			a++;

			//Destroy(telarañasAhorcado);

			//int i = 0;

			//Destroy( hangWebs );
			/*
			for( i = 0; i < hangWebs.transform.childCount ; i++ ){
				Vector3 t = hangWebs.transform.GetChild(i).position;
				t.y = float.MinValue;
				hangWebs.transform.GetChild(i).position = t;

				decalsController.gameObject.SendMessage("UpdateProjector" , hangWebs.transform.GetChild(i).GetComponent<DS_DecalProjector>() );
			}
			*/

			/*for( i = 0; i < chuletasWebs.transform.childCount ; i++ ){
				//Vector3 t = chuletasWebs.transform.GetChild(i).position;
				//t.y = float.MinValue;
				//chuletasWebs.transform.GetChild(i).position = t;
				
				decalsController.gameObject.SendMessage("UpdateProjector" , chuletasWebs.transform.GetChild(i).GetComponent<DS_DecalProjector>() );
			}*/

			/*
			for( i = 0; i < panel2_1.transform.childCount ; i++ ){
				if( panel2_1.transform.GetChild(i).gameObject.tag == "AhorcadoElemnts"){
					Vector3 t = panel2_1.transform.GetChild(i).position;
					t.y = float.MinValue;
					panel2_1.transform.GetChild(i).position = t;
					panel2_1.gameObject.SendMessage("UpdateProjector" , panel2_1.transform.GetChild(i).GetComponent<DS_DecalProjector>() );
				}
			}

			for( i = 0; i < panel2_2.transform.childCount ; i++ ){
				if( panel2_2.transform.GetChild(i).gameObject.tag == "AhorcadoElemnts"){
					Vector3 t = panel2_2.transform.GetChild(i).position;
					t.y = float.MinValue;
					panel2_2.transform.GetChild(i).position = t;
					panel2_2.gameObject.SendMessage("UpdateProjector" , panel2_2.transform.GetChild(i).GetComponent<DS_DecalProjector>() );
				}
			}
			*/

			//decalsController.UpdateDecalsMeshes( dm );


			//Edelweiss.DecalSystemEditor.EditorDecalsCreator.UpdateAllProjectors (decalsController);
			//decalsController.UpdateDecalsMeshes(dm);
			//telarañasAhorcado.gameObject.SetActive (false);
		//}
		
	

		if (cercaDeLibrero == true) {
			
			if (segundaConv == 0) {
				
				nuevoItemString = "Precio: 600 derechitos";
				informacionString = "Presiona C para comprar";
				if (Input.GetKeyDown ("c")) {
					
					if(derechitosNumeros >= 600)
					{
						
						personajeDiceString = "";
						nuevoItemString = "Felicitaciones has comprado un librero";
						derechitosNumeros = derechitosNumeros - 600;
						informacionString = "";
						Instantiate(particlebuy, librero.transform.position, Quaternion.identity);
						librero.gameObject.SetActive (false);
						cercaDeLibrero = false;
						Invoke("desaparecer",4);
						StartCoroutine("setMoney");
						
					}
					else
					{
						
						cercaDeLibrero = false;
						nuevoItemString = "Fondos Insuficientes";
						informacionString = "";
					}
					//aldeanoPensativoObjeto.SendMessage("noCamina");
					
					
				}
				
			}
		}
		if (cercaDeEscritorio == true) {
			
			if (segundaConv == 0) {
				
				nuevoItemString = "Precio: 700 derechitos";
				informacionString = "Presiona C para comprar";
				if (Input.GetKeyDown ("c")) {
					
					if(derechitosNumeros >= 700)
					{
						
						personajeDiceString = "";
						nuevoItemString = "Felicitaciones has comprado un Escritorio";
						derechitosNumeros = derechitosNumeros - 700;
						informacionString = "";
						Instantiate(particlebuy, escritorio.transform.position, Quaternion.identity);
						escritorio.gameObject.SetActive (false);
						cercaDeEscritorio = false;
						Invoke("desaparecer",4);
						StartCoroutine("setMoney");
						
					}
					else
					{
						
						cercaDeEscritorio = false;
						nuevoItemString = "Fondos Insuficientes";
						informacionString = "";
					}
					//aldeanoPensativoObjeto.SendMessage("noCamina");
					
					
				}
				
			}
		}
		if (cercaDePlanta2 == true) {
			
			if (segundaConv == 0) {
				
				nuevoItemString = "Precio: 200 derechitos";
				informacionString = "Presiona C para comprar";
				if (Input.GetKeyDown ("c")) {
					
					if(derechitosNumeros >= 200)
					{
						
						personajeDiceString = "";
						nuevoItemString = "Felicitaciones has comprado una Planta";
						derechitosNumeros = derechitosNumeros - 200;
						informacionString = "";
						Instantiate(particlebuy, planta2.transform.position, Quaternion.identity);
						planta2.gameObject.SetActive (false);
						cercaDePlanta2 = false;
						Invoke("desaparecer",4);
						StartCoroutine("setMoney");
						
					}
					else
					{

						cercaDePlanta2 = false;
						nuevoItemString = "Fondos Insuficientes";
						informacionString = "";
					}
					//aldeanoPensativoObjeto.SendMessage("noCamina");
					
					
				}
				
			}
		}

		if (cercaDePlanta1 == true) {
			
			if (segundaConv == 0) {
				
				nuevoItemString = "Precio: 200 derechitos";
				informacionString = "Presiona C para comprar";

				if (Input.GetKeyDown ("c")) {
					
					if(derechitosNumeros >= 200)
					{
						
						personajeDiceString = "";
						nuevoItemString = "Felicitaciones has comprado una Planta";
						derechitosNumeros = derechitosNumeros - 200;
						informacionString = "";
						Instantiate(particlebuy, planta1.transform.position, Quaternion.identity);
						planta1.gameObject.SetActive (false);
						cercaDePlanta1 = false;
						Invoke("desaparecer",4);
						StartCoroutine("setMoney");
						
					}
					else
					{
						nuevoItemString = "Fondos Insuficientes";
						informacionString = "";
						cercaDePlanta1 = false;
					
					}
					//aldeanoPensativoObjeto.SendMessage("noCamina");
					
					
				}
				
			}
		}




		if (cercaDeCama == true) {

			if (segundaConv == 0) {
				
				nuevoItemString = "Precio: 500 derechitos";
				informacionString = "Presiona C para comprar";
				if (Input.GetKeyDown ("c")) {
					
					if(derechitosNumeros >= 500)
					{

						personajeDiceString = "";
						nuevoItemString = "Felicitaciones has comprado una Cama";
						derechitosNumeros = derechitosNumeros - 500;
						informacionString = "";
						Instantiate(particlebuy, cama.transform.position, Quaternion.identity);
						cama.gameObject.SetActive (false);
						cercaDeCama = false;
						Invoke("desaparecer",4);
						StartCoroutine("setMoney");

					}
					else
					{
						nuevoItemString = "Fondos Insuficientes";
						informacionString = "";
						cercaDeCama = false;

			
					}
					//aldeanoPensativoObjeto.SendMessage("noCamina");
					
					
				}
				
			}
		}

		if (cercaDeBuro == true) {
			
			if (segundaConv == 0) {
				
				nuevoItemString = "Precio: 400 derechitos";
				informacionString = "Presiona C para comprar";
				if (Input.GetKeyDown ("c")) {
					
					if(derechitosNumeros >= 400)
					{
						
						personajeDiceString = "";
						nuevoItemString = "Felicitaciones has comprado una Buró";
						derechitosNumeros = derechitosNumeros - 400;
						informacionString = "";
						Instantiate(particlebuy, buro.transform.position, Quaternion.identity);
						buro.gameObject.SetActive (false);
						cercaDeBuro = false;
						Invoke("desaparecer",4);
						StartCoroutine("setMoney");
					}
					else
					{
						nuevoItemString = "Fondos Insuficientes";
						informacionString = "";
						cercaDeBuro = false;
						
					}
					//aldeanoPensativoObjeto.SendMessage("noCamina");
					
					
				}
				
			}
		}
		
		if (inicio)
		{
			instru.gameObject.SetActive(true);
			informacionString = "Presiona barra de espacio para continuar";
			if (Input.GetKeyDown(KeyCode.Space) ||  Input.GetTouch(0).phase == TouchPhase.Began)
			{
				if (Input.GetTouch(0).tapCount >= 2)
				{
					instru.SetActive(false);
					inicio = false;
					informacionString = "";
				}
			}
		}
        
		/// 
		/// 
		/// 
		/// 
		/// 
		/// 
		if (cercaDeMago == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				//mujerPreocupada.SendMessage ("camina");
				segundaConv = 2;
				
				/////////////
				/// /////////////ESCENA MAGO
				Application.ExternalCall("changeScene","4");
				back.SetActive(true);
				SendMessage("StopMusic");
				/// ///////////// ESCENA MAGO
				/// /////////////
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el Mago";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Mago";
					dialogoString = "Estas listo para cumplir tu destino?";
					informacionString = "Presiona F para continuar";
					//mujerPreocupada.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}



		if (cercaHundido == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				//mujerPreocupada.SendMessage ("camina");
				segundaConv = 2;
				
				/////////////
				/// /////////////ESCENA HUNDIDO
				GanasteMinijuego(2); 
				back.SetActive(true);
				Application.ExternalCall("changeScene","2");

				/// ///////////// ESCENA HUNDIDO
				/// /////////////
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con Sirviente de Ovat";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Hombre Retador";
					dialogoString = "Te reto, ¿será que puedes salvar a este hombre?";
					informacionString = "Presiona F para empezar el reto";
					//mujerPreocupada.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}


		if (cercaRetoChuletas == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				//mujerPreocupada.SendMessage ("camina");
				segundaConv = 2;

				/////////////
				/// /////////////ESCENA CHULETAS
				GanasteMinijuego(1); 
				back.SetActive(true);
				Application.ExternalCall("changeScene","1");
				/// ///////////// ESCENA CHULETAS
				/// /////////////
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el Hombre";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Hombre Retador";
					dialogoString = "Te reto, no creo que puedas salvar toda esta comida";
					informacionString = "Presiona F para empezar el reto";
					//mujerPreocupada.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}

		if (cercaNiñoEncerrado == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				niñoPerdidoObject.SendMessage ("camina");
				segundaConv = 2;
				if(haSalvadoAlNiñoEncerrado == false)
				{
					nuevoItemString = "Tus buenas acciones te han dado 200 derechitos";
					Invoke("desaparecer",4);
					derechitosNumeros = derechitosNumeros + 200;
				}

				haSalvadoAlNiñoEncerrado = true;
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el niño";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Niño Encerrado:";
					dialogoString = "Ovat me encerró aquí por que no le gusta mi risa...  Tengo estos derechitos es todo lo que puedo hacer por usted. ";
					if(haSalvadoAlNiñoEncerrado == true)
					{
						dialogoString = "Muchas gracias";
					}
					informacionString = "Presiona F para Finalizar la conversación";
					//niñoPerdidoObject.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}

		if (cercaDeAldeanoCansado2 == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				//mujerPreocupada.SendMessage ("camina");
				segundaConv = 2;
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el Hombre";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Hombre Agotado:";
					dialogoString = "Un pequeño descanzo no le hace mal a nadie";
					informacionString = "Presiona F para Finalizar la conversación";
					//mujerPreocupada.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}



		if (cercaDeAldeanoCansado == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				//mujerPreocupada.SendMessage ("camina");
				segundaConv = 2;
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el Hombre";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Hombre Cansado:";
					dialogoString = "Desde que Ovat está aqui tengo que trabajar horas extra...";
					informacionString = "Presiona F para Finalizar la conversación";
					//mujerPreocupada.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}


		if (cercaDeMujerPreocupada == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				mujerPreocupada.SendMessage ("camina");
				segundaConv = 2;
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con la Mujer";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Mujer Preocupada:";
					dialogoString = "Que será de mis hijos??  Mientras Ovat siga aqui no podre despreocuparme...";
					informacionString = "Presiona F para Finalizar la conversación";
					mujerPreocupada.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}

		if (cercaNiñoPerdido == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				niñoPerdidoObject.SendMessage ("camina");
				segundaConv = 2;
				if(tieneLLavedDeLaTorre == false)
				{
					nuevoItemString = "Tienes la llave del lugar mas alto de la aldea,  busca ese lugar y salva al hermano de este pequeño.";
					Invoke("desaparecer",4);
				}

				tieneLLavedDeLaTorre = true;
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el niño";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Niño en busqueda de su hermano:";
					dialogoString = "Ovat encerró a mi hermano. Todo lo que se es que esta en el lugar mas alto de la aldea y que necesito esta llave.  ¿Será que por favor podrías ayudarme? ";
if(tieneLLavedDeLaTorre == true)
{
dialogoString = "Por favor encuentra el lugar y usa la llave que te di para salvarlo..";
}
					informacionString = "Presiona F para Finalizar la conversación";
					niñoPerdidoObject.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			
		}
		if (cercaDeAldeanoPensativo == true) {
			
								//informacionString = "Presiona A para Hablar con el comerciante";
			
								if (Input.GetKeyDown ("f") && segundaConv == 1) {
										cuadroDeDialogo.gameObject.SetActive (false);
										dialogoString = "";
										informacionString = "";
										personajeDiceString = "";
										aldeanoPensativoObjeto.SendMessage ("camina");
										segundaConv = 2;

								}
			
			
								if (segundaConv == 0) {
										informacionString = "Presiona F para Hablar con el aldeano";
										if (Input.GetKeyDown ("f")) {
												cuadroDeDialogo.gameObject.SetActive (true);
												personajeDiceString = "Aldeano Pensativo:";
												dialogoString = "No se para que vivimos...";
												informacionString = "Presiona F para Finalizar la conversación";
												aldeanoPensativoObjeto.SendMessage ("noCamina");
												segundaConv ++;
					
										}
				
								}
			
						}


		if (cercaNiñaCuriosa == true) {
			
			//informacionString = "Presiona A para Hablar con el comerciante";
			
			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				niñaJuguetonaObjeto.SendMessage ("camina");
				segundaConv = 2;
				
			}
			
			
			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con la Joven";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Joven Enamorada:";
					dialogoString = "¿Será que el tambien me ama?";
					informacionString = "Presiona F para Finalizar la conversación";
					niñaJuguetonaObjeto.SendMessage ("noCamina");
					segundaConv ++;
					
				}
				
			}
			

		}





		if (cercaDeNiñaJuguetona == true) {

			//informacionString = "Presiona A para Hablar con el comerciante";

			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				niñaJuguetonaObjeto.SendMessage ("camina");
				segundaConv = 2;

			}


			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con la niña";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Niña Juguetona:";
					dialogoString = "Solo quiero Jugar Jugar Jugar...";
					informacionString = "Presiona F para Finalizar la conversación";
					niñaJuguetonaObjeto.SendMessage ("noCamina");
					segundaConv ++;

				}

			}

		}









		if (cercaDeMercader4 == true) {

			//informacionString = "Presiona A para Hablar con el comerciante";

			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				segundaConv = 2;

			}


			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el comerciante";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Comerciante Justo:";
					dialogoString = "Mi unico deseo es que Ovat se vaya...";
					informacionString = "Presiona F para Finalizar la conversación";
					segundaConv ++;

				}

			}

		}





		if (cercaDeMercader3 == true) {

			//informacionString = "Presiona A para Hablar con el comerciante";

			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				segundaConv = 2;

			}


			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el comerciante";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Comerciante Erick:";
					dialogoString = "Odio a Ovat, ha hecho demasiadas cosas malas";
					informacionString = "Presiona F para Finalizar la conversación";
					segundaConv ++;

				}

			}

		}














		if (cercaDeMercader2 == true) {

			//informacionString = "Presiona A para Hablar con el comerciante";

			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				segundaConv = 2;

			}


			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el comerciante";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Comerciante Juan:";
					dialogoString = "Ovat Sufrio mucho cuando era pequeño";
					informacionString = "Presiona F para Finalizar la conversación";
					segundaConv ++;

				}

			}

		}


	






		if (cercaDeMercader1 == true) {

			//informacionString = "Presiona A para Hablar con el comerciante";

			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				personajeDiceString = "";
				segundaConv = 2;

			}


			if (segundaConv == 0) {
				informacionString = "Presiona F para Hablar con el comerciante";
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					personajeDiceString = "Comerciante Pedro:";
					dialogoString = "Ovat No siempre fue malo";
					informacionString = "Presiona F para Finalizar la conversación";
					segundaConv ++;

				}

			}

		}
		////////// puerta torre
		/// 
		if (haAbiertoPuertaTorre == false) {
			if (cercaDePuertaTorre == true) { 


				informacionString = "Presiona F para abrirLaPuerta";
				 
				if (Input.GetKeyDown ("f")) {
					if (tieneLLavedDeLaTorre == false)
					{
						//cambioPuertaDecir = true;
						cercaDePuertaTorre = false;
						informacionString = "Necesitas la llave de la torre";
					}
					else
					{
						informacionString = "";
						//yaAbrioLaPuerta = true;
						nuevoItemString = "";
						haAbiertoPuertaTorre = true;
						cercaDePuertaTorre = false;
						puertaTorre.SendMessage ("initRotate");
						//puerta2.SendMessage ("initRotate");

					}
				}
			}

		}



		//////////////

		if (yaAbrioLaPuerta == false) {
			if (cercaDePuerta == true) { 
				if (cambioPuertaDecir == false) {
					informacionString = "Presiona F para abrirLaPuerta";
				} else {
					informacionString = "Necesitas tener una llave";
				}
				if (Input.GetKeyDown ("f")) {
					if (tieneLlave == false) {
						cambioPuertaDecir = true;
					} else {
						informacionString = "";
						yaAbrioLaPuerta = true;
						nuevoItemString = "";
						//initPuerta();
						puerta1.SendMessage ("initRotate");
						puerta2.SendMessage ("initRotate");
					}
				}
			}

		}


		if (cercaDeAldeana == true && tieneLlave == true) {
			informacionString = "Presiona F para Hablar con Marcela";

			if (Input.GetKeyDown ("f") && segundaConv == 1) {
				cuadroDeDialogo.gameObject.SetActive (false);
				dialogoString = "";
				informacionString = "";
				segundaConv = 2;

			}


			if (segundaConv == 0) {
				if (Input.GetKeyDown ("f")) {
					cuadroDeDialogo.gameObject.SetActive (true);
					if(controladorVictorias[2] == true)
						dialogoString = "Gracias por ayudarnos";
					else
						dialogoString = "los malechores estan en el segundo piso de la casa detras de mi";

					informacionString = "Presiona F para Finalizar la conversación";
					segundaConv ++;

				}

			}


			if (segundaConv == 2) {
				segundaConv = 0;
								}


		}

		if (segundaConv == 2) {
			segundaConv = 0;
		}


		if (cercaDeAldeana == true && tieneLlave == false) {



			if (finalizarDialogoMarcela == true && Input.GetKeyDown ("f")) {
				nuevoItemString = "Has obtenido la llave de la casa, ve a la casa y ayuda a esas personas";
				cuadroDeDialogo.gameObject.SetActive (false);
				informacionString = "";
				personajeDiceString = "";
				dialogoString = "";
				tieneLlave = true;

			}

			if (segundoDialogoDeMarcela == true && Input.GetKeyDown ("f")) {
				if (finalizarDialogoMarcela == false) {
					dialogoString = "Acaso nos podrías ayudar?   los Malechores están en el 2ndo piso, han capturado a mis hermanas   Aqui está la llave.";
					informacionString = "Presiona F para Finalizar la conversación";
					//nuevoItemString = "Has obtenido la llave de la casa, ve a la casa y ayuda a esas";
					finalizarDialogoMarcela = true;
				}
			}







			if (inicioDialogoConMarcela == false)
				informacionString = "Presiona F para Hablar con Marcela";
			else {
				if (segundoDialogoDeMarcela == false) {
					cuadroDeDialogo.gameObject.SetActive (true);
					informacionString = "Presiona F para Continuar la conversación";
					dialogoString = "¡Auxilio,auxilio! Unos malechores entraron en nuestra casa. ¡No hay nadie que nos ayude!";
					personajeDiceString = "Aldeana Marcela";
					segundoDialogoDeMarcela = true;
				}
			}

			if (Input.GetKeyDown ("f")) {
				inicioDialogoConMarcela = true;

			}
		}


		diariosString = "Diarios : "+ fragmentosDiario + "/30";
		derechitosString = "Derechitos: " + derechitosNumeros;
		dialogo.GetComponentInChildren<UILabel> ().text = dialogoString;
		informacion.GetComponentInChildren<UILabel> ().text = informacionString;
		nuevoItem.GetComponentInChildren<UILabel> ().text = nuevoItemString;
		personajeDice.GetComponentInChildren<UILabel> ().text = personajeDiceString;
		derechitosLabel.GetComponentInChildren<UILabel> ().text = derechitosString;
		diariosLabel.GetComponentInChildren<UILabel> ().text = diariosString;

								
	}
    
	void noDispararleAldeanos()
	{
		nuevoItemString = "Las redes no deben caer en los inocentes";
		Invoke("desaparecer",4);

	}

	void minionsCapturados(int minion)
	{

		derechitosNumeros = derechitosNumeros + 200;
		StartCoroutine("setMoney");
	}
		
	void OnTriggerExit(Collider other)
	{
		if (other.name == "planta")
		{
			cercaDePlanta1 = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			nuevoItemString = "";
		}
		
		if (other.name == "planta2")
		{
			cercaDePlanta2 = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			nuevoItemString = "";
		}
		
		if (other.name == "crateDesk")
		{
			cercaDeEscritorio = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			nuevoItemString = "";
		}
		
		if (other.name == "libroCrate")
		{
			cercaDeLibrero = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			nuevoItemString = "";
		}
		
		if (other.name == "buro")
		{
			cercaDeBuro = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			nuevoItemString = "";
		}
		if (other.name == "cama")
		{
			cercaDeCama = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			nuevoItemString = "";
		}

		
		/// mujerPreocupada
		/// 
		if (other.name == "niñoEncerrado")
		{
			cercaNiñoEncerrado = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//niñoPerdidoObject.SendMessage("camina");
		}

		if (other.name == "niñaCuriosa")
		{
			cercaNiñaCuriosa = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//niñoPerdidoObject.SendMessage("camina");
		}

		if (other.name == "niñoPedido")
		{
			cercaNiñoPerdido = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			niñoPerdidoObject.SendMessage("camina");
		}
		if (other.name == "retoChuletas")
		{
			cercaRetoChuletas = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//mujerPreocupada.SendMessage("camina");
		}
		if (other.name == "minion_Hundido")
		{
			cercaHundido = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
		}
		if (other.name == "aldeanoCansado")
		{
			cercaDeAldeanoCansado = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//mujerPreocupada.SendMessage("camina");
		}
		if (other.name == "aldeanoCansado2")
		{
			cercaDeAldeanoCansado2 = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//mujerPreocupada.SendMessage("camina");
		}
		if (other.name == "mujerPreocupada")
		{
			cercaDeMujerPreocupada = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			mujerPreocupada.SendMessage("camina");
		}
		if (other.name == "niñaJuguetona") 
		{
			
			cercaDeNiñaJuguetona = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//niñaJuguetonaObjeto.SendMessage("camina");
			
		}
		if (other.name == "niñoPerdido")
		{
			cercaNiñoPerdido = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//mujerPreocupada.SendMessage("camina");
		}

		if (other.name == "aldeanoPensativo") 
		{
			
			cercaDeAldeanoPensativo = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//aldeanoPensativoObjeto.SendMessage("camina");
		}

		if (other.name == "mago") 
		{
			
			cercaDeMago = false;
			dialogoString = "";
			informacionString = "";
			personajeDiceString = "";
			segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
			//aldeanoPensativoObjeto.SendMessage("camina");
		}



		if (other.name == "mercader1") 
		{
			
			cercaDeMercader1 = false;
            dialogoString = "";
		    informacionString = "";
		    personajeDiceString = "";
            segundaConv = 0;
            cuadroDeDialogo.gameObject.SetActive (false);

		}
		
		if (other.name == "mercader2") 
		{
			
			cercaDeMercader2 = false;
            dialogoString = "";
		    informacionString = "";
		    personajeDiceString = "";
            segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
		}
		
		
		if (other.name == "mercader3") 
		{
            dialogoString = "";
 		    informacionString = "";
		    personajeDiceString = "";
            segundaConv = 0;
			cercaDeMercader3 = false;
			cuadroDeDialogo.gameObject.SetActive (false);
		}
		
		
		if (other.name == "mercader4") 
		{
			cercaDeMercader4 = false;
            dialogoString = "";
		    informacionString = "";
		    personajeDiceString = "";
            segundaConv = 0;
			cuadroDeDialogo.gameObject.SetActive (false);
		}


		if (other.name == "AldeanaMarcela")
		{
			cercaDeAldeana = false;


			dialogoString = "";
		    informacionString = "";
			//nuevoItemString = "";
		    personajeDiceString = "";
            cuadroDeDialogo.gameObject.SetActive (false);
            


			inicioDialogoConMarcela = false;
		    segundoDialogoDeMarcela = false;
		    finalizarDialogoMarcela = false;
			//totalEndConversation = false;
               segundaConv = 0;

		}
		if (other.name == "towerDoor") {
			cercaDePuertaTorre = false;
			informacionString = "";
				}
		if (other.name == "puertaDoble1")
		{
			cercaDePuerta = false;
			informacionString = "";

		}

		}
    
	void OnTriggerEnter(Collider other)
	{

		if (other.name == "mujerPreocupada")
		{cercaDeMujerPreocupada = true;
		}
		if (other.name == "aldeanoCansado")
		{
			cercaDeAldeanoCansado = true;
		}
		if (other.name == "aldeanoCansado2")
		{
			cercaDeAldeanoCansado2 = true;
		}

		if (other.name == "planta")
		{
			cercaDePlanta1 = true;
		}

		if (other.name == "planta2")
		{
			cercaDePlanta2 = true;
		}

		if (other.name == "crateDesk")
		{
			cercaDeEscritorio = true;
		}

		if (other.name == "libroCrate")
		{
			cercaDeLibrero = true;
		}

		if (other.name == "buro")
		{
			cercaDeBuro = true;
		}
		if (other.name == "cama")
		{
			cercaDeCama = true;
		}




		if (other.name == "derechitos1")
		{
            diarios[0] = true;
			//cercaDiario1 = true;
		}
		if (other.name == "derechitos2")
		{
            diarios[1] = true;
            //cercaDiario2 = true;
		}
		if (other.name == "derechitos3")
		{
            diarios[2] = true;
            //cercaDiario3 = true;
		}
		if (other.name == "derechitos4")
		{
            diarios[3] = true;
            //cercaDiario4 = true;
		}
		if (other.name == "derechitos5")
		{
            diarios[4] = true;
            //cercaDiario5 = true;
		}
		if (other.name == "derechitos6")
		{
            diarios[5] = true;
            //cercaDiario6 = true;
		}
		if (other.name == "derechitos7")
		{
            diarios[6] = true;
            //cercaDiario7 = true;
		}
		if (other.name == "derechitos8")
		{
            diarios[7] = true;
            //cercaDiario8 = true;
		}
		if (other.name == "derechitos9")
		{
            diarios[8] = true;
            //cercaDiario9 = true;
		}
		if (other.name == "derechitos10")
		{
            diarios[9] = true;
            //cercaDiario10 = true;
		}
		if (other.name == "derechitos11")
		{
            diarios[10] = true;
            //cercaDiario11 = true;
		}
		if (other.name == "derechitos12")
		{
            diarios[11] = true;
            //cercaDiario12 = true;
		}
		if (other.name == "derechitos13")
		{
            diarios[12] = true;
            //cercaDiario13 = true;
		}
		if (other.name == "derechitos14")
		{
            diarios[13] = true;
            //cercaDiario14 = true;
		}
		if (other.name == "derechitos15")
		{
            diarios[14] = true;
            //cercaDiario15 = true;
		}
		if (other.name == "derechitos16")
		{
            diarios[15] = true;
            //cercaDiario16 = true;
		}
		if (other.name == "derechitos17")
		{
            diarios[16] = true;
            //cercaDiario17 = true;
		}
		if (other.name == "derechitos18")
		{
            diarios[17] = true;
            //cercaDiario18 = true;
		}

		if (other.name == "derechitos19")
		{
            diarios[18] = true;
            //cercaDiario19 = true;
		}
		if (other.name == "derechitos20")
		{
            diarios[19] = true;
            //cercaDiario20 = true;
		}
		if (other.name == "derechitos21")
		{
            diarios[20] = true;
            //cercaDiario21 = true;
		}
		if (other.name == "derechitos22")
		{
            diarios[21] = true;
            //cercaDiario22 = true;
		}
		if (other.name == "derechitos23")
		{
            diarios[22] = true;
            //cercaDiario23 = true;
		}
		if (other.name == "derechitos24")
		{
            diarios[23] = true;
            //cercaDiario24 = true;
		}
		if (other.name == "derechitos25")
		{
            diarios[24] = true;
            //cercaDiario25 = true;
		}
		if (other.name == "derechitos26")
		{
            diarios[25] = true;
            //cercaDiario26 = true;
		}
		if (other.name == "derechitos27")
		{
            diarios[26] = true;
            //cercaDiario27 = true;
		}
		if (other.name == "derechitos28")
		{
            diarios[27] = true;
            //cercaDiario28 = true;
		}
		if (other.name == "derechitos29")
		{
            diarios[28] = true;
            //cercaDiario29 = true;
		}
		if (other.name == "derechitos30")
		{
            diarios[29] = true;
            //cercaDiario30 = true;
		}


































		//cercaDeAldeana = false;
		//cercaDePuerta = false;
		if (other.name == "cuadroDetector")
		{

			//////////jb aqui cargas la escena en el momento en que toca este colider inicia la escena nueva
			/// //////////////////////////////////////////////////////////////////////
			/// 
			GanasteMinijuego(3);
			back.SetActive(true);
			Application.ExternalCall("changeScene","3");
		
		}
		if (other.name == "minion_Hundido")
		{
			cercaHundido=true;
		}


		if (other.name == "niñoEncerrado")
		{
			cercaNiñoEncerrado= true;
			Debug.Log("NIÑO ENCERRADO");
		}

		if (other.name == "towerDoor")
		{
			cercaDePuertaTorre= true;
		}


		if (other.name == "niñoPerdido")
		{
			cercaNiñoPerdido = true;
		}
		if (other.name == "retoChuletas")
		{
			cercaRetoChuletas=true;
		}
		if (other.name == "aldeanoPensativo")
		{
			cercaDeAldeanoPensativo = true;
		}
		if (other.name == "niñaJuguetona") 
		{
			cercaDeNiñaJuguetona = true;
		}


		if (other.name == "mercader1") 
		{
			//informacionString = "Presiona A para Hablar con Marcela";
			cercaDeMercader1 = true;
		}

		if (other.name == "mercader2") 
		{
			//informacionString = "Presiona A para Hablar con Marcela";
			cercaDeMercader2 = true;
		}


		if (other.name == "mercader3") 
		{
			//informacionString = "Presiona A para Hablar con Marcela";
			cercaDeMercader3 = true;
		}


		if (other.name == "mercader4") 
		{
			//informacionString = "Presiona A para Hablar con Marcela";
			cercaDeMercader4 = true;
		}

		if (other.name == "niñaCuriosa") 
		{
			//informacionString = "Presiona A para Hablar con Marcela";
			cercaNiñaCuriosa = true;
		}

		if (other.name == "mago") 
		{
			//informacionString = "Presiona A para Hablar con Marcela";
			cercaDeMago = true;
		}


		if (other.name == "AldeanaMarcela") 
		{
						//informacionString = "Presiona A para Hablar con Marcela";
			cercaDeAldeana = true;
		}
		else
		{
			informacionString = "";
			cercaDeAldeana = false;

		}


		if (other.name == "puertaDoble1") 
		{
			//informacionString = "Presiona A para abrirLaPuerta";
			cercaDePuerta = true;
		}
		else
		{
			informacionString = "";
			//informacionString = "";
			cercaDePuerta = false;
			cambioPuertaDecir = false;
		}
		
	}
}