using UnityEngine;
using System.Collections;

public class RangoTower2 : MonoBehaviour {

	public int segments;
	public float xradius;
	public float yradius;
	LineRenderer line;
	float tiempoInicial, tiempoActual;
	public float cadencia;
	// Use this for initialization
	void Start () {
		tiempoInicial = Time.time;
		line = gameObject.GetComponent<LineRenderer>();
		
		line.SetVertexCount (segments + 1);
		line.useWorldSpace = false;
		CreatePoints ();
		line.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerStay(Collider c)
	{
		if (c.gameObject.tag == "Enemy"|| c.gameObject.tag == "EnemyBoss") {
			tiempoActual =Time.time;
			if(tiempoActual-tiempoInicial>cadencia)
			{
				SendMessageUpwards("Shoot",c.gameObject,SendMessageOptions.DontRequireReceiver);
				tiempoInicial = tiempoActual;
			}
		}
	}

	void CreatePoints ()
	{
		float x;
		float y = 0f;
		float z;
		
		float angle = 20f;
		
		for (int i = 0; i < (segments + 1); i++)
		{
			x = Mathf.Sin (Mathf.Deg2Rad * angle) * xradius;
			z = Mathf.Cos (Mathf.Deg2Rad * angle) * yradius;
			
			line.SetPosition (i,new Vector3(x,y,z) );
			
			angle += (360f / segments);
		}
	}


}
