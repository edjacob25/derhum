﻿using UnityEngine;
using System.Collections;

public class RestartButtons : MonoBehaviour {
	void OnClick () {
		if (gameObject.name == "Main") {
			Application.ExternalCall("sendStatus","1");
			Application.ExternalCall("changeScene","0");
		}
		else {
			Variables.winner = true;
			Application.LoadLevel("game");
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
