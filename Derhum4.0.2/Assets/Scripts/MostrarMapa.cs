﻿using UnityEngine;
using System.Collections;

public class MostrarMapa : MonoBehaviour {

	public GameObject canvasMapa;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c) {
		if (c.gameObject.tag == "Player") {
			canvasMapa.SetActive(true);
			Destroy(gameObject);
		}
	}
}
