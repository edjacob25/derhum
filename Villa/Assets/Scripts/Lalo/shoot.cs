﻿using UnityEngine;
using System.Collections;

public class shoot : MonoBehaviour {

	// Use this for initialization
	public GameObject prefab; 
	
	void Start () {
		
	}
	
	void Update () {
		
		if (Input.GetButtonDown ("Fire1")&& Variables.redes > 0) {
			
			GameObject bullet = Instantiate (prefab, transform.position, Quaternion.identity) as GameObject;
			
			Physics.IgnoreCollision(transform.root.GetComponent<Collider>(), bullet.GetComponent<Collider>());
			
			Vector3 dir = transform.TransformDirection(Vector3.forward);
			
			bullet.GetComponent<Rigidbody>().AddForce(dir*100, ForceMode.Impulse);
			Variables.redes--;
			
		}
		
	}
}

