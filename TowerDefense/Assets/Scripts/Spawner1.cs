﻿using UnityEngine;
using System.Collections;

public class Spawner1 : MonoBehaviour {

	public GameObject enemigo1;
	public GameObject enemigo2;
	public GameObject enemigo3;
	public GameObject enemigo4;
	public GameObject lugar2;
	public float tiempo1;
	public int cantidad1;
	public float tiempo2;
	public int cantidad2;
	public float tiempo3;
	public int cantidad3;
	public float tiempo4;
	public int cantidad4;
	public GameObject jugador;
	public bool iniciar=false;
	float contadorEnemigos=0,enemigosRestantes=0,enemigostotales=0;
	int ronda = 1;
	PlayerStats stats;
	Preguntas preg;
	connectDB conexion;
	int contEspacio=0;
	GameObject EnemigoCopy;
	// Use this for initialization
	void Start () {
		stats = GameObject.Find ("ControlUI").GetComponent<PlayerStats> ();
		preg = GameObject.Find ("ControlPanel").GetComponent<Preguntas> ();
		conexion = GameObject.Find ("ControlPanel").GetComponent<connectDB> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (enemigosRestantes < 0)
			enemigosRestantes = 0;
		if (enemigosRestantes != 0)
			iniciar = false;
		if (iniciar&&enemigosRestantes==0) {
			if(ronda==1)
			{
				contadorEnemigos=0;
				contEspacio=1;
				Ronda1();
			}
			if(ronda==2)
			{
				contadorEnemigos=0;
				contEspacio=1;
				Ronda2();

			}
			if(ronda==3)
			{
				contadorEnemigos=0;
				Ronda3();
			}
			
			if(ronda==4)
			{
				contadorEnemigos=0;
				Ronda4();
			}
			
			if(ronda==5)
			{
				contadorEnemigos=0;
				contEspacio=1;
				Ronda5();
			}
			ronda++;

			iniciar=false;

		}
	}
	void Ronda1()
	{
		enemigostotales = cantidad1;
		if (contadorEnemigos < enemigostotales)
		{
			if(contEspacio==1)
				SpawnEnemy1(2);
			else{
				SpawnEnemy1(1);
				contEspacio=0;
			}
			enemigosRestantes++;
			Invoke ("Ronda1", tiempo1);
			contEspacio++;


		}
		contadorEnemigos++;

	}
	void Ronda2()
	{
		enemigostotales = cantidad2;
		if (contadorEnemigos < enemigostotales)
		{
			if(contEspacio==1)
				SpawnEnemy2(2);
			else{
				SpawnEnemy2(1);
				contEspacio=0;
			}
			enemigosRestantes++;
			Invoke ("Ronda2", tiempo2);
			contEspacio++;
		}
		contadorEnemigos++;
	}
	void Ronda3()
	{
		int creados=0;
		enemigostotales = cantidad3;
		if (contadorEnemigos < enemigostotales)
		{
			SpawnEnemy1(2);
			SpawnEnemy1 (1);
			if(contEspacio==2)
			{
				SpawnEnemy2(2);
				contEspacio=0;
				enemigosRestantes+=1;
				creados+=1;
			}
			else
			{
				SpawnEnemy2(1);
				enemigosRestantes+=1;
				creados+=1;
			}
			enemigosRestantes+=2;
			contEspacio++;
			creados+=2;
			Invoke ("Ronda3", tiempo3);
			
		}
		contadorEnemigos+=creados;
		
	}
	void Ronda4()
	{
		int creados=0;
		enemigostotales = cantidad4;
		if (contadorEnemigos < enemigostotales)
		{
			if(contEspacio%2==0)
				SpawnEnemy3(2);
			else
				SpawnEnemy3(1);
			if(contEspacio==10)
			{
				SpawnEnemy4(2);
				contEspacio=0;
				enemigosRestantes+=1;
				creados+=1;
			}
			enemigosRestantes++;
			contEspacio++;
			creados+=1;
			Invoke ("Ronda4", tiempo4);
			
		}
		contadorEnemigos++;
	}
	void Ronda5()
	{
		enemigostotales = cantidad4;
		if (contadorEnemigos < enemigostotales)
		{
			SpawnEnemy4(2);
			enemigosRestantes++;
			Invoke ("Ronda4", tiempo4);
			
		}
		contadorEnemigos++;
	}

	void SpawnEnemy1 (int lugar)
	{
		if(lugar==1)
			EnemigoCopy = Instantiate (enemigo1, this.transform.position, Quaternion.identity) as GameObject; 
		else 
			EnemigoCopy = Instantiate (enemigo1, lugar2.transform.position, Quaternion.identity) as GameObject; 

	}

	void SpawnEnemy2(int lugar)
	{
		if(lugar==1)
			EnemigoCopy = Instantiate (enemigo2, this.transform.position, Quaternion.identity) as GameObject; 
		else 
			EnemigoCopy = Instantiate (enemigo2, lugar2.transform.position, Quaternion.identity) as GameObject; 
	}

	void SpawnEnemy3(int lugar)
	{
		if(lugar==1)
			EnemigoCopy = Instantiate (enemigo3, this.transform.position, Quaternion.identity) as GameObject; 
		else 
			EnemigoCopy = Instantiate (enemigo3, lugar2.transform.position, Quaternion.identity) as GameObject; 
	}
	void SpawnEnemy4(int lugar)
	{
		if(lugar==1)
			EnemigoCopy = Instantiate (enemigo4, this.transform.position, Quaternion.identity) as GameObject; 
		else 
			EnemigoCopy = Instantiate (enemigo4, lugar2.transform.position, Quaternion.identity) as GameObject; 
	}


	public void IniciarRonda()
	{
		iniciar = true;
	}
	public void ReducirContador()
	{
		enemigosRestantes--;
		if (enemigostotales <= contadorEnemigos && enemigosRestantes <= 0) {
			jugador.SendMessage ("TerminarJuego");
			conexion.question();
			if(ronda!=5)
				preg.CambiarPanel(2);
			stats.CambiaRonda (ronda);

		}
	}
}
