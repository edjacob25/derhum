﻿using UnityEngine;
using System.Collections;

public class TimeLabel : MonoBehaviour {
	
	float timeleft = 0.1f;
	float targetTime;
	

	void SetTime()
	{
		targetTime = Time.time + 10;
		timeleft = 0.01f;
	}

	void Update () {
		if (timeleft > 0)
			timeleft = targetTime - Time.time;
		else 
			timeleft = 0;
		gameObject.GetComponent<UILabel>().text = "Tiempo: " + timeleft.ToString("F");;
	}
}
