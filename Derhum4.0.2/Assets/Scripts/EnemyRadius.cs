﻿using UnityEngine;
using System.Collections;

public class EnemyRadius : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	void OnTriggerEnter(Collider c)
	{
		
		//Debug.Log(c.gameObject);

		if (c.gameObject.tag == "Player")
		{
			this.transform.parent.gameObject.SendMessage("StartFollow");
		}

		
	}
	
	void OnTriggerExit(Collider c)
	{
		
		//Debug.Log(c.gameObject);

		if (c.gameObject.tag == "Player")
		{
			this.transform.parent.gameObject.SendMessage("StopFollow");
		}
	}
	
	
	
}