﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {
	public int points = 0;
	public int fallenChulets = 0;
	public int bombsReceived = 0;
	public int items;
	//public Communication comm;

	public void AddPoint()
	{
		points++;
		//Debug.Log("Puntos: " + points);
	}

	public void Bomb(int negs){
		points = points - negs;
		bombsReceived ++;
	}
	public void Fallen(){
		fallenChulets ++;
	}

	public void UpdateScore()
	{
		Variables.accumulatedScore = points;
	}
	                       
	// Use this for initialization
	void Start () {
		//items = comm.getData("Items");
		points = Variables.accumulatedScore;
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(0.7f);
		Application.LoadLevel("Pregunta");
	}

	// Update is called once per frame
	void Update () {
		if((fallenChulets > 3) && (bombsReceived > 1))
		{
			//UpdateScore();
			Variables.winner = false;
			StartCoroutine("Wait");
			//Debug.Log("espera");
		
		}
	}
}
