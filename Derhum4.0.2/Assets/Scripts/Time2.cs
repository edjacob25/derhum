﻿using UnityEngine;
using System.Collections; 

public class Time2 : MonoBehaviour {

	public GameObject loseText;
	private float time = 360;
	private bool c = false;
	public GameObject canvasHealth;
	
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(time <= 0)
		{
			time = 0;
			loseText.SetActive(true);
		}
		
		
	}
	
	
	void Crono()
	{
		time = time - 1;
		Invoke ("Crono", 1);
	}
	
	void OnGUI(){
		
		if (c == true) 
		{
			canvasHealth.SendMessage("Time2", time, SendMessageOptions.DontRequireReceiver);
			//GUI.Label(new Rect(5,0,100,100) , "Time Left: " + time  ); 
		}
		
		
	}
	
	void OnTriggerEnter(Collider c){
		
		if (c.gameObject.tag == "Time") 
		{
			Invoke ("Crono", 0);
			Invoke ("TimeStart",0);
		}
		
	}
	
	void TimeStart()
	{
		c = true;
	}
}
